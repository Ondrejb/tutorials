﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //lblName.Text = "Hello World";
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            var email = new MailMessage()
            {
                Subject = ("Email from EmailForm"),
                Body = txtMessage.Text,
                IsBodyHtml = false,
                From = new MailAddress(txtEmail.Text, txtName.Text)
            };
            email.To.Add(new MailAddress("st39703@student.upce.cz", "Ondřej Beneš"));

            //nastaveni serveru v web.config
            var smtp = new SmtpClient();
            smtp.EnableSsl = true;
            smtp.Send(email);
            pnlSuccess.Visible = true;
            ClearForm();
        }
        catch (Exception ex)
        {
            pnlError.Visible = true;
            ErrorMessage.Text = ex.Message;
        }
    }

    private void ClearForm()
    {
        foreach (var control in form1.Controls)
        {
            if (control is TextBox)
            {
                TextBox textBox = control as TextBox;
                textBox.Text = "";
            }
        }
    }
}