﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Email Form</title>
    <link href="Style.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">    
     <div>
        <asp:Panel ID="pnlSuccess" runat="server" Visible="false">
            <p>Your Email was sent successfully.</p>
        </asp:Panel>
         <asp:Panel ID="pnlError" runat="server" Visible="false">
            <p>Your Email was not sent due to an error.</p>
             <p><asp:Literal ID="ErrorMessage" runat="server" /></p>
        </asp:Panel>
        <asp:Label CssClass="txt" AssociatedControlID="txtName" ID="lblName" runat="server" Text="Name"></asp:Label>
        <div>
            <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
        </div>
        <asp:Label AssociatedControlID="txtEmail" ID="lblEmail" runat="server" Text="Email"></asp:Label>
        <div>
            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
        </div>
        <asp:Label AssociatedControlID="txtMessage" ID="lblMessage" runat="server" Text="Message"></asp:Label>
        <div>
            <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Rows="20"></asp:TextBox>
        </div>
        <div>
            <asp:Button ID="btnSubmit" runat="server" Text="Send Message" OnClick="btnSubmit_Click"/>
        </div>
    </div>
    </form>
</body>
</html>
