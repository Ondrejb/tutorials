﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DataStore
/// </summary>
public static class DataStore
{
    public static IEnumerable<string> GetNames()
    {
        return new string[] {
            "Pepa",
            "Jarda",
            "Franc"
        };
    }
}