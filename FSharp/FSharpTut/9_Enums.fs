﻿module enum
open System

type emotion =
| joy = 0
| fear = 1
| anger = 2

let enum_stuff() =
    let my_feeling = emotion.joy

    match my_feeling with
    | joy -> printfn "I'm joyful"
    | fear -> printfn "I'm fearful"
    | anger -> printfn "I'm angry"

//enum_stuff()
//
//Console.ReadKey() |> ignore