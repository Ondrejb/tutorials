﻿module binding
open System

let bind_stuff() =
    // data is by default immutable
    let mutable weight = 175
    weight <- 170

    printfn "Weight: %i" weight

    let change_me = ref 10
    change_me := 50

    // working with references - have to use !
    printfn "Change: %i" ! change_me

//bind_stuff()
//
//Console.ReadKey() |> ignore