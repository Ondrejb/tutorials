﻿module math
open System

let do_math() =
    // power
    printfn "5 ** 2 = %.1f" (5.0 ** 2.0)

    let number = 2
    printfn "Type: %A" (number.GetType())

    printfn "A float: %.2f" (float number)
    printfn "An int: %i" (int 3.54)

    // Some math funcs
    printfn "abs -4.5: %f" (abs -4.5)
    printfn "log 2.71828: %f" (log 2.71828)
    printfn "sqrt 25: %f" (sqrt 25.0)

do_math()

Console.ReadKey() |> ignore