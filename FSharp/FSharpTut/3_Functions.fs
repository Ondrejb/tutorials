﻿module functions
open System

let do_funcs() =
    // func def with types
    let get_sum(x: int, y: int): int = x + y

    printfn "5 + 7 = %i" (get_sum(5,7))


    // recursive func
    let rec factorial x =
        if x < 1 then 1
        else x * factorial (x-1)

    printfn "Factorial 4: %i" (factorial 4)

    
    // lambda
    let rand_list = [1;2;3]
    let rand_list2 = List.map (fun x -> x*2) rand_list

    printfn "Double list: %A" rand_list2


    // pipeline
    [5;6;7;8]
    |> List.filter (fun v -> (v % 2) = 0)
    |> List.map (fun x -> x * 2)
    |> printfn "Even Doubles: %A"


    // multiple funcs ("chaining")
    let mul_num x = x * 3
    let add_num x = x + 5

    let mul_add = mul_num >> add_num
    let add_mul = mul_num << add_num

    printfn "mul_add: %i" (mul_add 10)
    printfn "add_mul: %i" (add_mul 10)

let square x = x * x
let isOdd x = x % 2 <> 0

let getSquaredOfOdds nums =
    nums |> List.filter isOdd |> List.map (fun x-> (x, square x))

let main argv =
    let numbers = [1 .. 25]
    let squaresOfOdds = getSquaredOfOdds numbers
    
    for (num, square) in squaresOfOdds do
        printfn "%d squared is %d" num square

main()
//do_funcs()

Console.ReadKey() |> ignore