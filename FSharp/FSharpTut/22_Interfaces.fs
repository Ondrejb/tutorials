﻿module interfaces
open System

type IAddingService =
    abstract member Add: int -> int -> int

type MyAddingService() =
    
    interface IAddingService with 
        member this.Add x y = 
            x + y

    interface System.IDisposable with 
        member this.Dispose() = 
            printfn "disposed"

let testAddingService (adder:IAddingService) = 
    printfn "1+2=%i" <| adder.Add 1 2  // ok

let mas = new MyAddingService()

testAddingService mas // cast automatically

Console.ReadKey() |> ignore

