﻿module execptions
open System

let ex_stuff() =
    let divide x y =
        try
            if y = 0 then raise(DivideByZeroException "Can't devide by 0")
            printfn "%i / %i = %i" x y (x / y)
        with
            | :? DivideByZeroException -> 
            printfn "Can't divide by zero"

    divide 5 4
    divide 5 0

//ex_stuff()
//
//Console.ReadKey() |> ignore