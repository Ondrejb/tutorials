﻿module map
open System

let map_stuff() =
    let customers =
        Map.empty.
            Add("Bob Smith", 100.50).
            Add("Ally Marks", 11.50).
            Add("Tim Burton", 10.0)

    printfn "# of Customers: %i" customers.Count

    let cust = customers.TryFind "Bob Smith"

    match cust with
    | Some x -> printfn "Balance: %.2f" x
    | None -> printfn "Not Found"

    printfn "Customers: %A" customers

    if customers.ContainsKey "Bob Smith" then
        printfn "Bob found"

    printfn "Bob's Balance: %.2f" customers.["Bob Smith"]

    let cust2 = Map.remove "Ally Marks" customers
    
    printfn "# of Customers: %i" cust2.Count

//map_stuff()
//
//Console.ReadKey() |> ignore