﻿module p
open System

module Parsing =
    let tryParseWith tryParseFunc s =
        match (tryParseFunc s) with
        | true, v -> Some v
        | false, _ -> None

    let parseDateTime s = tryParseWith System.DateTime.TryParse s
    let parseDouble d = tryParseWith System.Double.TryParse d

open Parsing

let parsedTime = parseDateTime "2009-09-28"
printfn "%A" parsedTime

let parsedDouble = parseDouble "14,64"
printfn "%A" parsedDouble

Console.ReadKey() |> ignore