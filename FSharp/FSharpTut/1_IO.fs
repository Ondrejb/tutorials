﻿module io
open System

let hello() =
    printf "Enter your name: "

    let name = Console.ReadLine()

    // %i %f %b %A (itnernal repr.) %O (object) %M
    printfn "Hello %s" name

let precision() =
    printfn "PI : %f" 3.141592653589793238462643383

    let big_pi = 3.141592653589793238462643383M

    printfn "Big PI: %M" big_pi

let padding() =
    printfn "%-5s %5s" "a" "b"

    // dynamic padding
    printfn "%*s" 10 "Hi"



// hello()
// precision()
// padding()

// Console.ReadKey() |> ignore