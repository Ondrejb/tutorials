﻿module measures
open System

[<Measure>] type Minute
[<Measure>] type Second

let addMinutes (currentTime: int<Minute>) (mins: int<Minute>) = currentTime + mins

// addMinutes 40 12 -> Compile error - don't know what we are adding
// addMinutes 40<Minute> 12<Second> -> Compile error - adding minutes and seconds
let m = addMinutes 40<Minute> 12<Minute>
printfn "%d" m
Console.ReadKey() |> ignore

