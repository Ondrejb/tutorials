﻿module sequence
open System

let seq_stuff() =
    let seq1 = seq { 1 .. 100 }
    let seq2 = seq { 0 .. 2 .. 50} // even nums from 0 to 50
    let seq3 = seq { 50 .. 1}

    // Won't be completely generated until needed
    printfn "Seq2: %A" seq2

    Seq.toList seq2 |> List.iter (printfn "Num: %i")

    let is_prime n =
        let rec check i =
            i > n/2 || (n % i <> 0 && check (i + 1))
        check 2

    let prime_seq = seq { for n in 1..500 do if is_prime n then yield n}
    
    Seq.toList prime_seq |> List.iter (printfn "Num: %i")

//seq_stuff()
//
//Console.ReadKey() |> ignore