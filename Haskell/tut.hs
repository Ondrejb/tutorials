 {-
    Comment
 -}
 
 -- Comment
 
 -- ghci
 -- :l soubor
 -- :r
 
 import Data.List
 import System.IO
 
 maxInt = maxBound :: Int
 
 sumOfNums = sum [1..1000]
 
 add = 5 + 4
 addNeg = 5 + (-4)
 
 modEx = mod 5 4
  
 num9 = 9 :: Int
 sqrtOf9 = sqrt(fromIntegral num9)
 
 squared9 = 9**2

 trueAndFalse = True && False
 
 primeNumbers = [3, 5, 7, 9]
 
 morePrimes = primeNumbers ++ [13, 17, 19, 23, 29]
 
 favNums = 2 : 7 : 21 : 66 : []
 
 morePrimes2 = 2 : morePrimes
 
 lenPrime = length morePrimes2
 
 revPrime = reverse morePrimes2
 
 isListEmpty = null morePrimes2
 
 secondPrime = morePrimes2 !! 1
 
 firstPrime = head morePrimes2
 
 lastPrime = last morePrimes2
 
 primeInit = init morePrimes2
 
 first3Primes = take 3 morePrimes2
 
 removedPrimes = drop 3 morePrimes2
 
 is7InList = 7 `elem` morePrimes2
 
 maxPrime = maximum morePrimes2
 
 zeroToTen = [0..10]

 evenList = [2, 4..20]
 
 infinPow10 = [10, 20..]
 
 many2s = take 10(repeat 2)
 
 listTime3 = [x * 3 | x <- [1..10], x * 3 <= 20]
 
 divisBy9N13 = [x | x <- [1..500], x `mod`13 == 0, x `mod`9 == 0]
 
 sortedList = sort [9, 1, 8, 6]
 
 sumOfLists = zipWith (+) [1, 2, 3, 4, 5] [6, 7, 8, 9, 10]
 
 listBiggerThen5 = filter (>5) morePrimes
 
 evensUpTo20 = takeWhile (<=20) [2, 4..]
 
 multOfList = foldl (*) 1 [2, 3, 4, 5]
 
 pow3List = [3 ** n | n <- [1..10]]
 
 multTable = [[x * y | y <- [1..10]] | x <- [1..10]]
 
 randTuple = (1, "Random Tuple")
 
 bobSmith = ("Bob Smith", 52)
 
 bobsName = fst bobSmith
 
 bobsAge = snd bobSmith
 
 names = ["Bob", "Mary", "Tom"]
 
 addresses = ["1233 Main", "234 North", "567 South"]
 
 namesAndAddresses = zip names addresses
 
 {--
 main = do
    putStrLn "What's your name"
    name <- getLine
    putStrLn ("Hello " ++ name)
 --}
 
 -- funcName param1 param2 ... paramN = operations (return value)

 addMe :: Int -> Int -> Int

 addMe x y = x + y
  
 addTuples :: (Int, Int) -> (Int, Int) -> (Int, Int)
 
 addTuples (x, y)(x2, y2) = (x + x2, y + y2)
 
 whatAge :: Int -> String 
 whatAge 16 = "You can drive" 
 whatAge 18 = "You can vote" 
 whatAge 21 = "You are an adult" 
 whatAge x = "Nothing important"
 
 factorial :: Integer -> Integer 
 factorial 0 = 1
 factorial n = n * factorial (n-1)
 
 isOdd :: Int -> Bool
 
 isOdd n
    | n `mod` 2 == 0 = False
    | otherwise = True
 
 whatGrade :: Int -> String
 whatGrade age
    | (age >= 5) && (age <= 6) = "Kindergarten"
    | (age > 6) && (age <= 10) = "Elementary School"
    | (age > 10) && (age <= 14) = "Middle School"
    | (age > 14) && (age <= 18) = "High School"
    | otherwise = "Go to college"
 
 batAvgRation :: Double -> Double -> String
 batAvgRation hits atBats
    | avg <= 0.200 = "Terrible avg"
    | avg <= 0.250 = "Avg"
    | otherwise = "Super"
    where avg = hits / atBats
 
 getListItems :: [Int] -> String
 getListItems [] = "Your list is empty"
 getListItems (x:[]) = "Your list starts with " ++ show x
 getListItems (x:y:[]) = "Your list containts " ++ show x ++ show y
 getListItems (x:xs) = "Your 1st item is " ++ show x ++ " and the rest is " ++ show xs
 
 getFirstItem :: String -> String
 getFirstItem [] = "Empty String"
 getFirstItem all@(x:xs) = "The first letter in " ++ all ++ " is " ++ [x]
 
 times4 :: Int -> Int
 times4 x = x * 4
 
 listTimes4 = map times4 [1,2,3,4,5]
 
 multBy4 :: [Int] -> [Int]
 multBy4 [] = []
 multBy4 (x:xs) = times4 x : multBy4 xs
 
 areStringsEq :: [Char] -> [Char] -> Bool
 areStringsEq [] [] = True
 areStringsEq (x:xs)(y:ys) = x == y && areStringsEq xs ys
 areStringsEq _ _ = False
 
 doMult :: (Int -> Int) -> Int
 doMult func = func 3
 
 num3Times4 = doMult times4
 
 getAddFunc :: Int -> (Int -> Int)
 getAddFunc x y = x + y
 
 adds3 = getAddFunc 3
 
 fourPlus3 = adds3 4
 
 -- lambda
 dbl1To10 = map (\x -> x * 2) [1..10]
 
 -- if
 doubleEvenNumber y = 
    if (y `mod` 2 /= 0)
        then y
        else y * 2
 
 -- switch
 getClass :: Int -> String
 getClass n = case n of
    5 -> "Go to Kindergarten"
    6 -> "Go to elem school"
    _ -> "Go away"
 
  -- enum
 data BaseballPlayer = 
    Pitcher | Catcher | Infielder | Outfield
    deriving Show -- pouzitelne jako String
 
 barryBonds :: BaseballPlayer -> Bool
 barryBonds Outfield = True
 barryInOF = print(barryBonds Outfield)
 
 -- "struct"
 data Customer = Customer String String Double
    deriving Show

 tomSmith :: Customer
 tomSmith = Customer "Tom Smith" "123 Main" 20.50
 
 getBalance :: Customer -> Double
 getBalance (Customer _ _ b) = b
 
 data RPS = Rock | Paper | Scissors
 
 shoot :: RPS -> RPS -> String
 shoot Paper Rock = "Paper beats Rock"
 -- ostatni kombinace
 shoot _ _ = "Error"
 
 -- "dedicnost"
 data Shape = Circle Float Float Float | Rectangle Float Float Float Float
    deriving Show

 area :: Shape -> Float
 
 -- $ misto zavorek
 area (Circle _ _ r) = pi * r ^ 2
 area (Rectangle x y x2 y2) = (abs $ x2 - x) * (abs $ y2 - y)
 
 sumValue = putStrLn (show (1 + 2))
 
 sumValue2 = putStrLn . show $ 1 + 2
 
 data Employee = Employee { 
     name :: String, position :: String, idNum :: Int} 
     deriving (Eq, Show)
 
 sam = Employee {name = "Sam", position = "Manager", idNum = 1}
 pam = Employee {name = "Pam", position = "Janitor", idNum = 2}

 data ShirtSize = S | M | L
   
  -- "pretezovani"
 {--instance Eq ShirtSize where
    S == S = True
	M == M = True
	L == L = True
	_ == _ = False
	--}

 instance Show ShirtSize where
	show S = "Small"
    
	show M = "Medium"
	
	show L = "Large"
 
 -- zapis do souboru
 writeToFile = do
  theFile <- openFile "test.txt" WriteMode
  hPutStrLn theFile ("random line of text")
  hClose theFile
  
 readFromFile = do
  theFile2 <- openFile "test.txt" ReadMode
  contents <- hGetContents theFile2
  putStr contents
  hClose theFile2
  
  
  
  
  
  
  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 