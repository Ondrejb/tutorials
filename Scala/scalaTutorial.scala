import scala.io.StdIn.{readLine, readInt}
import scala.math._
import scala.collection.mutable.ArrayBuffer
import java.io.PrintWriter
import scala.io.Source


object ScalaTutorial {
	def main(args: Array[String]) {
		/*var i = 0	
	
		for (i <- 1 to 10)
			println(i)
			
		val randLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		
		for (i <- 0 until randLetters.length)
			println(randLetters(i))
			
		val aList = List(1,2,3,4,5)
		
		for (i <- aList)
			println("List items " + i)
			
		var evenList = for {i <- 1 to 20
			if (i % 2) == 0
			} yield i
			
		for (i <- evenList)
			println(i)
			
		for (i <- 1 to 5; j <- 6 to 10) {
			println("i : " + i)
			println("j : " + j)
		}
		
		
		def printPrimes() {
			val primeList = List(1,2,3,5,7,11)
			for(i <- primeList) {
				if (i == 11) {
					return
				}
			
				if(i != 1) {
					println(i)
				}			
			}		
		}
		
		printPrimes*/
		
		var numberGuess = 0
		
		do {
			print("Guess a number: ")
			numberGuess = readLine.toInt
		} while (numberGuess != 15)
		
		printf("You guessed %d\n", 15)
		
		val name = "Ondra"
		val age = 22
		val weight = 175.5
		
		println(s"Hello $name")
		println(f"I am ${age+1} and $weight%.2f")
		
		
	}
}