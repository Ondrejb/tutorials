#include <iostream>
#include <ctime>
#include <cuda.h>
#include <cuda_runtime.h>				// Stops underlining of __global__
#include <device_launch_parameters.h>	// Stops underlining of threadIdx etc.

using namespace std;

__global__ void FindClosestGPU(float3* points, int* indices, int count)
{
if(count <= 1) return;

int idx = threadIdx.x + blockIdx.x * blockDim.x;
if(idx < count)
	{
	float3 thisPoint = points[idx];
	float smallestSoFar = 3.40282e38f;
	
	for(int i = 0; i < count; i++)
		{
		if(i == idx) continue;

		float dist = (thisPoint.x - points[i].x)*(thisPoint.x - points[i].x);
		dist += (thisPoint.y - points[i].y)*(thisPoint.y - points[i].y);
		dist += (thisPoint.z - points[i].z)*(thisPoint.z - points[i].z);
		
		if(dist < smallestSoFar)
			{
			smallestSoFar = dist;
			indices[idx] = i;
			}
		}
	}
}

__device__ const int blockSize = 640;

__global__ void FindClosestGPUShared(float3* points, int* indices, int count)
{
	__shared__ float3 sharedPoints[blockSize];

	int idx = threadIdx.x + blockIdx.x * blockDim.x;

	if (count <= 1 || count > idx) return;

	int indexOfClosest = -1;

	float3 thisPoint = points[idx];

	float distanceToClosest = 3.40282e38f;

	// kopirovat z global do shared
	for (int currentBlockOfPoints = 0; currentBlockOfPoints < gridDim.x; currentBlockOfPoints++)
	{
		if (threadIdx.x + currentBlockOfPoints * blockSize < count)
			sharedPoints[threadIdx.x] = points[threadIdx.x + currentBlockOfPoints * blockSize];
		
		__syncthreads();

		float *ptr = &sharedPoints[0].x;

		for (int i = 0; i < blockSize; i++)
		{
			float dist = (thisPoint.x - ptr[0])*(thisPoint.x - ptr[0]);
			dist += (thisPoint.y - ptr[1])*(thisPoint.y - ptr[1]);
			dist += (thisPoint.z - ptr[2])*(thisPoint.z - ptr[2]);

			ptr += 3;

			int currentIndex = i + currentBlockOfPoints * blockSize;

			if ((dist < distanceToClosest) &&
				(currentIndex < count) &&
				(currentIndex != idx))
			{
				distanceToClosest = dist;
				indexOfClosest = currentIndex;
			}
		}
		__syncthreads();
		
		indices[idx] = indexOfClosest;
	}
}