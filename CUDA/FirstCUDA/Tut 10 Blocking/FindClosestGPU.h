#pragma once

__global__ void FindClosestGPU(float3* points, int* indices, int count);

__global__ void FindClosestGPUShared(float3* points, int* indices, int count);