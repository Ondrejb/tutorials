#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <iostream>

#include <thrust\device_vector.h>
#include <thrust\host_vector.h>
#include <thrust\sort.h>
#include <thrust\reduce.h>


using namespace std;

int main()
{
	thrust::device_vector<int> dv(0);
	thrust::host_vector<int> hv(0);

	cout << "TEST" << endl;

	for (int i = 0; i < 5; i++)
		hv.push_back(rand() % 101);

	thrust::copy(hv.begin(), hv.end(), dv.begin());

	thrust::sort(dv.begin(), dv.end());

	for (int i = 0; i < 5; i++)
		cout << "Item: " << i << " is " << dv[i] << endl;

	float sum = thrust::reduce(dv.begin(), dv.end());

	cout << "Sum: " << sum << endl;

	return 0;
}