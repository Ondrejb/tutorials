
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <ctime>

using namespace std;

__global__ void AddInts(int* a, int *b, int count)
{
	int id = blockIdx.x * blockDim.x + threadIdx.x;

	if (id < count)
	{
		a[id] += b[id];
	}

}

int main()
{
	srand(time(NULL));
	int count = 1000;

	int *h_a = new int[count];
	int *h_b = new int[count];

	for (int i = 0; i < count; i++)
	{
		h_a[i] = rand() % 1000;
		h_b[i] = rand() % 1000;
	}

	cout << "Prior to addition: " << endl;

	for (int i = 0; i < 5; i++)
	{
		cout << h_a[i] << " " << h_b[i] << endl;
	}

	int *d_a, *d_b;

	if (cudaMalloc(&d_a, sizeof(int) * count) != cudaSuccess)
	{
		cout << "Allocating d_a unsuccesfull";
		return 0;
	}

	if (cudaMalloc(&d_b, sizeof(int) * count) != cudaSuccess)
	{
		cout << "Allocating d_b unsuccesfull";
		cudaFree(d_a);
		return 0;
	}

	if (cudaMemcpy(d_a, h_a, sizeof(int) * count, cudaMemcpyHostToDevice) != cudaSuccess)
	{
		cout << "Memcpy d_a unsuccesfull";
		cudaFree(d_a);
		cudaFree(d_b);
		return 0;
	}

	if (cudaMemcpy(d_b, h_b, sizeof(int) * count, cudaMemcpyHostToDevice) != cudaSuccess)
	{
		cout << "Memcpy d_a unsuccesfull";
		cudaFree(d_a);
		cudaFree(d_b);
		return 0;
	}

	AddInts << <count / 256 + 1, 256 >> >(d_a, d_b, count);

	if (cudaMemcpy(h_a, d_a, sizeof(int) * count, cudaMemcpyDeviceToHost) != cudaSuccess)
	{
		delete[] h_a;
		delete[] h_b;

		cudaFree(d_a);
		cudaFree(d_b);

		cout << "Memcpy h_a unsuccesfull";

		return 0;
	}

	for (int i = 0; i < 5; i++)
	{
		cout << h_a[i] << endl;
	}

	cudaFree(d_a);
	cudaFree(d_b);

	delete[] h_a;
	delete[] h_b;

	return 0;

}