package cz.beny.android.fileservice;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class FileService extends IntentService {

    public static final String TRANSACTION_DONE = "cz.beny.android.TRANSACTION_DONE";

    public FileService() {
        super(FileService.class.getName());
    }

    public FileService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.e("FileService", "Service Started");
        String passedUrl = intent.getStringExtra("url");
        downloadFile(passedUrl);
        Log.e("FileService", "Service Completed");

        Intent i = new Intent(TRANSACTION_DONE);
        FileService.this.sendBroadcast(i);
    }

    protected void downloadFile(String url) {
        String fileName = "myFile";

        try {
            FileOutputStream os = openFileOutput(fileName, Context.MODE_PRIVATE);
            URL fileUrl = new URL(url);

            HttpsURLConnection urlConn = (HttpsURLConnection) fileUrl.openConnection();

            urlConn.setRequestMethod("GET");
            urlConn.setDoOutput(true);
            urlConn.connect();

            InputStream is = urlConn.getInputStream();
            byte[] buffer = new byte[1024];
            int bufferLen = 0;

            while((bufferLen = is.read(buffer)) > 0) {
                os.write(buffer, 0, bufferLen);
            }

            os.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
