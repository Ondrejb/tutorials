package cz.beny.android.savingdata;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Created by Admin on 21.8.2016.
 */
public class SettingsActivity extends PreferenceActivity {

    @SuppressWarnings("deprecation")

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);
    }
}
