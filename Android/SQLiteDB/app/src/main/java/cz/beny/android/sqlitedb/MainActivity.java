package cz.beny.android.sqlitedb;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    SQLiteDatabase contactsDb = null;

    Button createDBButton, addContactButton, deleteContactButton, getContactsButton,
            deleteDBButton;
    EditText nameEditText, emailEditText, contactListEditText, idEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        createDBButton = (Button) findViewById(R.id.createDBButton);
        addContactButton = (Button) findViewById(R.id.addContactButton);
        deleteContactButton = (Button) findViewById(R.id.deleteContactButton);
        getContactsButton = (Button) findViewById(R.id.getContactsButton);
        deleteDBButton = (Button) findViewById(R.id.deleteDBButton);

        nameEditText = (EditText) findViewById(R.id.nameEditText);
        emailEditText = (EditText) findViewById(R.id.emailEditText);
        contactListEditText = (EditText) findViewById(R.id.contactListEditText);
        idEditText = (EditText) findViewById(R.id.idEditText);
    }

    public void createDatabase(View view) {
        try {
            contactsDb = this.openOrCreateDatabase("MyContacts", MODE_PRIVATE, null);
            contactsDb.execSQL("CREATE TABLE IF NOT EXISTS CONTACTS" +
                    "(id integer primary key, name VARCHAR, email VARCHAR);");
            File db = getApplicationContext().getDatabasePath("MyContacts.db");
            if(db.exists()) {
                Toast.makeText(this, "Database Created", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Database Missing", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception ex) {
            Log.e("CONTACTS ERROR", "ERROR CREATING DB");
        }

        addContactButton.setClickable(true);
        deleteContactButton.setClickable(true);
        getContactsButton.setClickable(true);
        deleteDBButton.setClickable(true);
    }

    public void addContact(View view) {
        String contactName = nameEditText.getText().toString();
        String contactEmail = emailEditText.getText().toString();
        contactsDb.execSQL("INSERT INTO CONTACTS (name, email) VALUES ('" +
            contactName + "', '" + contactEmail + "');");
    }

    public void getContacts(View view) {
        Cursor cursor = contactsDb.rawQuery("SELECT * FROM CONTACTS", null);
        int idColumn = cursor.getColumnIndex("id");
        int nameColumn = cursor.getColumnIndex("name");
        int emailColumn = cursor.getColumnIndex("email");

        cursor.moveToFirst();

        String contactList = "";

        if(cursor != null && (cursor.getCount() > 0)) {
            do {
                String id = cursor.getString(idColumn);
                String name = cursor.getString(nameColumn);
                String email = cursor.getString(emailColumn);

                contactList = contactList + id + " : " + name + " : " + email + "\n";
            } while (cursor.moveToNext());
            contactListEditText.setText(contactList);
        } else {
            Toast.makeText(this, "No results to show", Toast.LENGTH_SHORT).show();
            contactListEditText.setText("");
        }
    }

    public void deleteContact(View view) {
        String id = idEditText.getText().toString();
        contactsDb.execSQL("DELETE FROM CONTACTS WHERE id = "
        + id + ";");
    }

    public void deleteDatabase(View view) {
        this.deleteDatabase("MyContacts");
    }

    @Override
    public void onDestroy() {
        contactsDb.close();
        super.onDestroy();
    }
}
