package cz.beny.android.fragments;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FragmentManager fm = getFragmentManager();

        FragmentTransaction ft = fm.beginTransaction();

        Configuration configInfo = getResources().getConfiguration();

        if(configInfo.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            FragmentLandscape fl  = new FragmentLandscape();
            ft.replace(android.R.id.content, fl);
        } else {
            FragmentPortrait fp  = new FragmentPortrait();
            ft.replace(android.R.id.content, fp);
        }

        ft.commit();

        // setContentView(R.layout.activity_main);
    }
}
