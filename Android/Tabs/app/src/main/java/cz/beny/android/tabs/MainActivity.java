package cz.beny.android.tabs;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.widget.Toast;

public class MainActivity extends Activity {

    ActionBar.Tab tab1, tab2, tab3;

    Fragment fragment1 = new TabFragment1();
    Fragment fragment2 = new TabFragment2();
    Fragment fragment3 = new TabFragment3();

    @SuppressWarnings("depracaion")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       // requestWindowFeature(Window.FEATURE_ACTION_BAR);

        setContentView(R.layout.activity_main);

        ActionBar actionBar = getActionBar();

        if(actionBar == null) {
            Toast.makeText(MainActivity.this, "", Toast.LENGTH_SHORT).show();
        }

        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        tab1 = actionBar.newTab().setText("Tab 1");
        tab2 = actionBar.newTab().setText("Tab 2");
        tab3 = actionBar.newTab().setText("Tab 3");

        tab1.setTabListener(new TabListener(fragment1));
        tab2.setTabListener(new TabListener(fragment2));
        tab3.setTabListener(new TabListener(fragment3));

        actionBar.addTab(tab1);
        actionBar.addTab(tab2);
        actionBar.addTab(tab3);
    }
}
