package cz.beny.android.smsmessaging;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText txtMsgEditText, pNumEditText, messagesEditText;
    Button sendButton;

    static String messages = "";

    Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtMsgEditText = (EditText) findViewById(R.id.txtMsgEditText);
        pNumEditText = (EditText) findViewById(R.id.pNumEditText);
        messagesEditText = (EditText) findViewById(R.id.messagesEditText);
        sendButton = (Button) findViewById(R.id.sendButton);

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(5000);
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                messagesEditText.setText(messages);
                            }
                        });
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    public void sendMessage(View view) {
        String phoneNum = pNumEditText.getText().toString();
        String message = txtMsgEditText.getText().toString();

        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNum, null, message, null, null);

            Toast.makeText(this, "Message sent", Toast.LENGTH_SHORT).show();
        } catch (IllegalArgumentException ex) {
            Toast.makeText(this, "Enter a number and a message", Toast.LENGTH_SHORT).show();
        }

        messages = messages + "You : " + message + "\n";
    }

    public static class SmsReciever extends BroadcastReceiver {
        final SmsManager smsManager = SmsManager.getDefault();

        public SmsReciever(){}

        @Override
        public void onReceive(Context context, Intent intent) {
            final Bundle bundle = intent.getExtras();

            try {
                if(bundle != null) {
                    final Object[] pdusObj = (Object[])bundle.get("pdus");

                    for(int i = 0; i < pdusObj.length; i++) {
                        SmsMessage smsMessage = SmsMessage.createFromPdu((byte[])pdusObj[i]);

                        String phoneNumber = smsMessage.getDisplayOriginatingAddress();
                        String message = smsMessage.getDisplayMessageBody();

                        message = messages + phoneNumber + " : " + message + "\n";
                    }
                }
            } catch (Exception ex) {
                Log.e("SmsReciever", "Exception SmsReciever");
            }
        }
    }

    public class MMSReciever extends BroadcastReceiver {

        public MMSReciever(){}

        @Override
        public void onReceive(Context context, Intent intent) {
            throw new UnsupportedOperationException("Not implemented yet");
        }
    }

    public class HeadlessSmsSendService extends BroadcastReceiver {

        public HeadlessSmsSendService(){}

        @Override
        public void onReceive(Context context, Intent intent) {
            throw new UnsupportedOperationException("Not implemented yet");
        }
    }

    @Override
    protected void onDestroy() {
        mHandler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }
}
