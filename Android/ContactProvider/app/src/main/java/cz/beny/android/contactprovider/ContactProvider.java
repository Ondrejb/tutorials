package cz.beny.android.contactprovider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.widget.Toast;

import java.util.HashMap;

/**
 * Created by Admin on 21.8.2016.
 */
public class ContactProvider extends ContentProvider {

    public static final String PROVIDER_NAME =
            "cz.beny.android.contactprovider.ContactProvider";
    public static final String URL = "content://" + PROVIDER_NAME + "/cpcontacts";
    public static final Uri CONTENT_URL = Uri.parse(URL);
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final int URI_CODE = 1;

    private static HashMap<String, String> values;

    static final UriMatcher URI_MATCHER;

    static {
        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        URI_MATCHER.addURI(PROVIDER_NAME, "cpcontacts", URI_CODE);
    }

    private SQLiteDatabase sqlDb;
    static final String DATABASE_NAME = "myContacts";
    static final String TABLE_NAME = "names";
    static final int DATABASE_VERSION = 1;
    static final String CREATE_DB_TABLE = " CREATE TABLE " + TABLE_NAME
            + " (id INTEGER PRIMARY KEY AUTOINCREMENT, "
            + " name TEXT NOT NULL);";

    @Override
    public boolean onCreate() {
        DatabaseHelper dbHelper = new DatabaseHelper(getContext());
        sqlDb = dbHelper.getWritableDatabase();

        return sqlDb != null;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(TABLE_NAME);
        switch (URI_MATCHER.match(uri)) {
            case URI_CODE:
                builder.setProjectionMap(values);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI" + uri);
        }

        Cursor cursor = builder.query(sqlDb, projection, selection, selectionArgs, null, null, sortOrder);

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        switch (URI_MATCHER.match(uri)) {
            case URI_CODE:
                return "vnd.android.curor.dir/cpcontacts";
            default:
                throw new IllegalArgumentException("Unsupported URI" + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        long rowId = sqlDb.insert(TABLE_NAME, null, contentValues);

        if(rowId > 0) {
            Uri _uri = ContentUris.withAppendedId(CONTENT_URL, rowId);
            getContext().getContentResolver().notifyChange(_uri, null);
            return _uri;
        } else {
            Toast.makeText(getContext(), "Row insert failed", Toast.LENGTH_SHORT).show();
            return null;
        }
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        int rowsDeleted = 0;
        switch (URI_MATCHER.match(uri)) {
            case URI_CODE:
                rowsDeleted = sqlDb.delete(TABLE_NAME, s, strings);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI" + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        int rowsUpdated = 0;

        switch (URI_MATCHER.match(uri)) {
            case URI_CODE:
                rowsUpdated = sqlDb.update(TABLE_NAME, contentValues, s, strings);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI" + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase sqlDb) {
            sqlDb.execSQL(CREATE_DB_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqlDb, int i, int i1) {
            sqlDb.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(sqlDb);
        }
    }
}
