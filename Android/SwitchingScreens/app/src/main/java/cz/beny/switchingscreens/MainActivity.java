package cz.beny.switchingscreens;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onGetNameClick(View view) {

        //Intent getNameScreenIntent = new Intent(this, SecondScreen.class);

        final int result = 1;

        Human bob = new Human(180, 90, "Bob");

        Intent sendBob = new Intent(this, SecondScreen.class);
        sendBob.putExtra("humanBob", bob);

        startActivityForResult(sendBob, result);

        //getNameScreenIntent.putExtra("callingActivity", "MainActivity");

        startActivityForResult(sendBob, result);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        TextView usersNameMessage = (TextView) findViewById(R.id.users_name_message);

        String nameSentBack = data.getStringExtra("usersName");

        usersNameMessage.append(" " + nameSentBack);

    }
}
