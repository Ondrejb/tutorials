package cz.beny.android.googlemaps;

import android.content.Intent;
import android.net.Uri;
import android.net.http.HttpResponseCache;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity {

    static final LatLng DerekPos = new LatLng(40, -79);

    private GoogleMap googleMap;

    EditText addressEditText, finalAddressEditText;
    LatLng addressPos, finalAddressPos;
    Marker addressMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addressEditText = (EditText) findViewById(R.id.addressEditText);
        finalAddressEditText = (EditText) findViewById(R.id.finalAddressEditText);

        try {
            if(googleMap == null) {
                googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
            }

            googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
            googleMap.setMyLocationEnabled(true);
            googleMap.setTrafficEnabled(true);
            googleMap.setIndoorEnabled(true);
            googleMap.setBuildingsEnabled(true);
            googleMap.getUiSettings().setZoomControlsEnabled(true);

            Marker marker = googleMap.addMarker(
                    new MarkerOptions().position(DerekPos).title("HELLO"));

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void showAddressMarker(View view) {
        String newAdress = addressEditText.getText().toString();

        if(newAdress != null) {
            new PlaceAMarker().execute(newAdress);
        }
    }

    public void getDirections(View view) {
        String startingAddress = addressEditText.getText().toString();
        String finalAddress = finalAddressEditText.getText().toString();

        if((startingAddress.equals("")) || (finalAddress.equals(""))){
            Toast.makeText(this, "Enter a Starting & Ending Address", Toast.LENGTH_SHORT).show();
        } else {
            new GetDirections().execute(startingAddress, finalAddress);
        }
    }

    class PlaceAMarker extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            String startAddress = params[0];

            startAddress = startAddress.replaceAll(" ", "%20");

            getLatLng(startAddress, false);

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            addressMarker =  googleMap.addMarker(
                    new MarkerOptions().position(addressPos).title("Address"));
        }
    }

    class GetDirections extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            String startAddress = params[0];
            startAddress = startAddress.replace(" ", "%20");
            getLatLng(startAddress, false);

            String endAddress = params[1];
            endAddress = endAddress.replaceAll(" ", "%20");
            getLatLng(endAddress, true);

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            String geoUriString = "http://maps.google.com/maps?addr=" +
                    addressPos.latitude + "," +
                    addressPos.longitude + "&daddr=" +
                    finalAddressPos.latitude + "," +
                    finalAddressPos.longitude;

            Intent mapCall = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUriString));

            startActivity(mapCall);
        }
    }

    protected void getLatLng(String address, boolean setDestination) {
        String uri = "http://maps.google.com/maps/api/geocode/json?address="
                + address + "&sensor=false";

        HttpGet httpGet = new HttpGet(uri);
        HttpClient client = new DefaultHttpClient();
        HttpResponse response;
        StringBuilder sb = new StringBuilder();

        try {
            response = client.execute(httpGet);
            HttpEntity entity = response.getEntity();
            InputStream is = entity.getContent();

            int byteData;
            while ((byteData = is.read()) != -1) {
                sb.append((char)byteData);
            }

            double lat = 0.0, lng = 0.0;

            JSONObject jsonObject;

            try {
                jsonObject = new JSONObject(sb.toString());

                lng = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
                        .getJSONObject("geometry").getJSONObject("location")
                        .getDouble("lng");

                lat = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
                        .getJSONObject("geometry").getJSONObject("location")
                        .getDouble("lat");

                if(setDestination) {
                    finalAddressPos = new LatLng(lat, lng);
                } else {
                    addressPos =  new LatLng(lat, lng);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
