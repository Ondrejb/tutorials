package cz.beny.messingaround;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button answerYesButton, answerNoButton;
    private EditText userNameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        answerYesButton = (Button) findViewById(R.id.answerYesButton);
        answerNoButton = (Button) findViewById(R.id.answerNoButton);
        userNameEditText = (EditText) findViewById(R.id.userNameEditText);
    }

    public void onYesButtonClick(View view) {
        String userName = String.valueOf(userNameEditText.getText());

        String yourYesResponse = "That is great, " + userName;

        Toast.makeText(this, yourYesResponse, Toast.LENGTH_SHORT).show();
    }

    public void onNoButtonClick(View view) {
        String userName = String.valueOf(userNameEditText.getText());

        String yourNoResponse = "Too bad, " + userName;

        Toast.makeText(this, yourNoResponse, Toast.LENGTH_LONG).show();

    }
}
