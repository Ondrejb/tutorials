import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

'''
Network for regression - finding a continous function based on data samples
'''

''' tensorflow:
cd "D:\Program Files\Python\Scripts" 
tensorboard --logdir="D:\ONDRA\Projekty\Python\TensorFlowExample\logs"
browser - http://admin-pc:6006/#graphs
'''

def add_layer(inputs, in_size, out_size, n_layer, activation_function = None):
    layer_name = "layer%s" % n_layer
    with tf.name_scope("layer"):
        with tf.name_scope("weights"):
            Weights = tf.Variable(tf.random_normal([in_size, out_size]), name='W')
            tf.summary.histogram(layer_name+"/weights", Weights)
        with tf.name_scope("bias"):
            biases = tf.Variable(tf.zeros([1, out_size]) + 0.1, name='biases')
            tf.summary.histogram(layer_name+"/biases", biases)
        with tf.name_scope("inputs"):
            Wx_plus_b = tf.matmul(inputs, Weights) + biases

        if activation_function is None:
            outputs = Wx_plus_b
        else:
            outputs = activation_function(Wx_plus_b)

        tf.summary.histogram(layer_name+"/outputs", outputs)
        return outputs

# generate data
x_data = np.linspace(-1, 1, 300)[:,np.newaxis] # matrix of 300 rows
noise = np.random.normal(0, 0.05, x_data.shape)
y_data = np.square(x_data) - 0.5 + noise

# visualize data
# plt.scatter(x_data, y_data)
# plt.show()

# define placeholder for inputs to network
with tf.name_scope("inputs"):
    xs = tf.placeholder(tf.float32, [None, 1], name='x_inputs') # 1 means only 1 feature
    ys = tf.placeholder(tf.float32, [None, 1], name='y_inputs')

# add hidden layer
l1 = add_layer(xs, 1, 10, " 0", activation_function = tf.nn.relu) # 10 neurons
l2 = add_layer(l1, 10, 10, " 1", activation_function = tf.nn.relu) # 10 neurons

# add output layer
prediction = add_layer(l2, 10, 1, " 2", activation_function = None)

# error
with tf.name_scope("loss"):
    loss = tf.reduce_mean(tf.reduce_sum(tf.square(ys - prediction), reduction_indices=[1]))

    tf.summary.scalar("loss", loss)
with tf.name_scope("train"):
    train_step = tf.train.GradientDescentOptimizer(0.1).minimize(loss)

# var init
init = tf.initialize_all_variables()
sess = tf.Session()

merged = tf.summary.merge_all()
writer = tf.summary.FileWriter("logs/", sess.graph)

sess.run(init)

# plot the real data
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.scatter(x_data, y_data)
plt.show(block=False)
#plt.ion()

# training
for i in range(1000):
    sess.run(train_step, feed_dict={xs:x_data, ys:y_data})

    if i % 50 == 0:
        # visualize improvement
        try:
            ax.lines.remove(lines[0]) # remove the line if it exists
        except Exception:
            pass

        prediction_value = sess.run(prediction, feed_dict={xs: x_data})
        result = sess.run(merged, feed_dict={xs:x_data, ys:y_data})

        writer.add_summary(result, i)

        # plot the prediction
        lines = ax.plot(x_data, prediction_value, 'r', lw = 5)
        plt.pause(0.1)

plt.show()
print("DONE")