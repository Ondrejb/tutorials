import tensorflow as tf
import numpy as np

# Trains the network to output data similar to y = 0.1x + 0.3
# Network is just one neuron?

# create data
x_data = np.random.rand(100).astype(np.float32)
y_data = x_data * 0.1 + 0.3

# create tensorflow structure
Weights = tf.Variable(tf.random_uniform([1], -1.0, 1.0))
biases = tf.Variable(tf.zeros([1]))

y = Weights * x_data + biases

loss = tf.reduce_mean(tf.square(y-y_data))
optimizer = tf.train.GradientDescentOptimizer(0.5)
train = optimizer.minimize(loss)

init = tf.initialize_all_variables() # important
# end of tf structure


sess = tf.Session()
sess.run(init)

# train the network 201 times
for step in range(201):
    sess.run(train)

    if step % 20 == 0:
        print(step, sess.run(Weights), sess.run(biases))