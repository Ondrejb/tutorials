import tensorflow as tf

input1 = tf.placeholder(tf.float32)
input2 = tf.placeholder(tf.float32)
output = tf.multiply(input1, input2)

with tf.Session() as sess:
    # pass 7 to placeholder 1 and 2 to placeholder 2
    print(sess.run(output, feed_dict={input1: [7.], input2: [2.]}))
