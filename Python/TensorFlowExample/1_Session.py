import tensorflow as tf

matrix1 = tf.constant([[3, 3]]) # matrix - 1 row, 2 cols
matrix2 = tf.constant([[2],     # matrix - 2 rows, 1 col
                       [2]])
product = tf.matmul(matrix1, matrix2) # matrix multiplication

# method 1
sess = tf.Session()
result = sess.run(product)
print(result)
sess.close() # may not be necessary

# method 2
with tf.Session() as s:
    result2 = s.run(product)
    print(result2)
    # session closes automatically
