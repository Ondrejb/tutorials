from tkinter import *
from tkinter import ttk
import sqlite3

class StudentDB:

    # Class Fields
    db_conn = 0
    theCursor = 0
    curr_student = 0

    def setup_db(self):
        print("Setup DB")

        # Open or create DB
        self.db_conn = sqlite3.connect('student.db')

        # Create the cursor
        self.theCursor = self.db_conn.cursor()
        try:
        # Create the table if it doesn't exist
            self.db_conn.execute("CREATE TABLE IF NOT EXISTS STUDENTS(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, FNAME TEXT NOT NULL, LNAME TEXT NOT NULL);")
            self.db_conn.commit() # commit after DDL?

        except Exception as ex:
            print("Error during db setup")
            print(ex.args)

    def stud_submit(self):
        print("Submit Student")

        # Insert the student into the DB
        self.db_conn.execute("INSERT INTO STUDENTS(FNAME, LNAME) VALUES('" + 
                             self.fn_entry_value.get() + "', '" + 
                             self.ln_entry_value.get() + "')")

        # Clear entry boxes
        self.fn_entry.delete(0, "end")
        self.ln_entry.delete(0, "end")

        # Update listbox
        self.update_listbox()
    
    def update_listbox(self):
        print("Update listbox")

        # Delete items
        self.list_box.delete(0, END)

        # Get students from db
        try:
            result = self.theCursor.execute("SELECT ID, FNAME, LNAME FROM STUDENTS")

            for row in result:
                stud_id = row[0]
                stud_fname = row[1]
                stud_lname = row[2]
                
                # Put students as items
                self.list_box.insert(stud_id,
                                     stud_fname + " " +
                                     stud_lname)
                
        except Exception as ex:
            print("Error during db read")
            print(ex.args)


    def load_student(self, event = None):
        print("Load student")

        # Get the index selected 
        lb_widget = event.widget
        index = str(lb_widget.curselection()[0] + 1)

        # Store the current student index
        self.curr_student = index

        # Retrieve student list from db
        try:
            result = self.theCursor.execute("SELECT ID, FNAME, LNAME FROM STUDENTS WHERE ID=" + index)

            # Should be just one row
            for row in result:
                stud_id = row[0]
                stud_fname = row[1]
                stud_lname = row[2]

                # Set the names in the entries
                self.fn_entry_value.set(stud_fname)
                self.ln_entry_value.set(stud_lname)

        except Exception as ex:
            print("Error during db read")
            print(ex.args)

    def update_student(self):
        print("Update Stud")

        # Update based on current student
        try:
            self.db_conn.execute("UPDATE STUDENTS SET FNAME = '"+
                                 self.fn_entry_value.get() + "' , LNAME='" + 
                                 self.ln_entry_value.get()+"' WHERE ID = " + 
                                 self.curr_student)

            self.db_conn.commit()
            
        except Exception as ex:
            print("Error during db update")
            print(ex.args)

        # Clear entries
        self.fn_entry.delete(0, "end")
        self.ln_entry.delete(0, "end")

        # Update LB with student list
        self.update_listbox()


    def __init__(self, root):
        root.title("Sudent Database")
        root.geometry("300x350")

        # ------ 1st Row ------
        fn_label = Label(root, text = "First Name")
        fn_label.grid(row = 0, column = 0, padx = 10, pady = 10, sticky = W)

        self.fn_entry_value = StringVar(root, value = "")
        self.fn_entry = ttk.Entry(root, textvariable = self.fn_entry_value)
        self.fn_entry.grid(row = 0, column = 1, padx = 10, pady = 10, sticky = W)

        # ------ 2nd Row ------
        ln_label = Label(root, text = "Last Name")
        ln_label.grid(row = 1, column = 0, padx = 10, pady = 10, sticky = W)

        self.ln_entry_value = StringVar(root, value = "")
        self.ln_entry = ttk.Entry(root, textvariable = self.ln_entry_value)
        self.ln_entry.grid(row = 1, column = 1, padx = 10, pady = 10, sticky = W)

        # ------ 3rd Row ------
        self.submit_button = ttk.Button(root, text = "Submit", command = lambda: self.stud_submit())
        self.submit_button.grid(row = 2, column = 0, padx = 10, pady = 10, sticky = W)
        
        self.update_button = ttk.Button(root, text = "Update", command = lambda: self.update_student())
        self.update_button.grid(row = 2, column = 1, padx = 10, pady = 10)
                
        # ------ 4th Row ------
        scrollbar = Scrollbar(root)

        self.list_box = Listbox(root)
        self.list_box.bind("<<ListboxSelect>>", self.load_student)
        self.list_box.insert(1, "Students Here")
        self.list_box.grid(row = 3, column = 0, columnspan = 4, padx = 10, pady = 10, sticky = W+E)


        # DB Setup
        self.setup_db()
        self.update_listbox()
        
root = Tk()

studDB = StudentDB(root)

root.mainloop()