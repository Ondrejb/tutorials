from tkinter import *
import tkinter.font

class PaintApp:
    drawing_tool = "pencil"
    left_but = "up"
    xpos, ypos = None, None
    x1_line_pt, y1_line_pt, x2_line_pt, y2_line_pt = None, None, None, None

    def change_tool(self, tool):
        self.drawing_tool = tool

    def left_btn_down(self, event = None):
        self.left_but = "down"
        self.x1_line_pt = event.x
        self.y1_line_pt = event.y

    def left_btn_up(self, event = None):
        self.left_but = "up"

        self.xpos = None
        self.ypos = None

        self.x2_line_pt = event.x
        self.y2_line_pt = event.y

        if self.drawing_tool == "line":
            self.line_draw(event)
        elif self.drawing_tool == "arc":
            self.arc_draw(event)
        elif self.drawing_tool == "oval":
            self.oval_draw(event)
        elif self.drawing_tool == "rectangle":
            self.rectangle_draw(event)
        elif self.drawing_tool == "text":
            self.text_draw(event)

    def pencil_draw(self, event = None):
        if self.left_but == "down":
            if self.xpos is not None and self.ypos is not None:
                event.widget.create_line(self.xpos, self.ypos, event.x, event.y, smooth = TRUE, fill = "red")

            self.xpos = event.x
            self.ypos = event.y

    def motion(self, event = None):
        if self.drawing_tool == "pencil":
            self.pencil_draw(event)

    def line_draw(self, event = None):
        if None not in (self.x1_line_pt, self.y1_line_pt, self.x2_line_pt, self.y2_line_pt):
            event.widget.create_line(self.x1_line_pt, self.y1_line_pt, self.x2_line_pt, self.y2_line_pt, smooth = TRUE, fill = "green")

    def arc_draw(self, event = None):
        if None not in (self.x1_line_pt, self.y1_line_pt, self.x2_line_pt, self.y2_line_pt):
            coords = self.x1_line_pt, self.y1_line_pt, self.x2_line_pt, self.y2_line_pt
            event.widget.create_arc(coords, start = 0, extent = 150, style = ARC, fill = "yellow")

    def oval_draw(self, event = None):
        if None not in (self.x1_line_pt, self.y1_line_pt, self.x2_line_pt, self.y2_line_pt):
            coords = self.x1_line_pt, self.y1_line_pt, self.x2_line_pt, self.y2_line_pt
            event.widget.create_oval(coords, outline = "yellow", width = 2, fill = "midnight blue")

    def rectangle_draw(self, event = None):
        if None not in (self.x1_line_pt, self.y1_line_pt, self.x2_line_pt, self.y2_line_pt):
            coords = self.x1_line_pt, self.y1_line_pt, self.x2_line_pt, self.y2_line_pt
            event.widget.create_rectangle(coords, outline = "yellow", width = 2, fill = "midnight blue")

    def text_draw(self, event = None):
        if None not in (self.x1_line_pt, self.y1_line_pt):
            text_font = tkinter.font.Font(family = "Helvetice", size = 0, weight = "bold", slant = "italic")
            event.widget.create_text(self.x1_line_pt, self.y1_line_pt, fill = "green", font = text_font, text = "WOW")
            
    def __init__(self, root):
        drawing_area = Canvas(root)
        drawing_area.pack()
        drawing_area.bind("<Motion>", self.motion)
        drawing_area.bind("<ButtonPress-1>", self.left_btn_down)
        drawing_area.bind("<ButtonRelease-1>", self.left_btn_up)

        the_menu = Menu(root)

        tool_menu = Menu(the_menu, tearoff = 0)

        
        tool = StringVar()
        tool.set("Pencil")

        tool_menu.add_radiobutton(label = "Pencil", variable = tool, command = lambda: self.change_tool("pencil"))
        tool_menu.add_radiobutton(label = "Line", variable = tool, command = lambda: self.change_tool("line"))
        tool_menu.add_radiobutton(label = "Arc", variable = tool, command = lambda: self.change_tool("arc"))
        tool_menu.add_radiobutton(label = "Oval", variable = tool, command = lambda: self.change_tool("oval"))
        tool_menu.add_radiobutton(label = "Rectangle", variable = tool, command = lambda: self.change_tool("rectangle"))
        tool_menu.add_radiobutton(label = "Text", variable = tool, command = lambda: self.change_tool("text"))

        the_menu.add_cascade(label = "Tool", menu = tool_menu)

        root.config(menu = the_menu)


root = Tk()

paint_app = PaintApp(root)

root.mainloop()