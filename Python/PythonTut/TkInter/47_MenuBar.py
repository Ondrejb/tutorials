from tkinter import *
from tkinter import ttk
from tkinter import messagebox

def quit_app():
    root.quit()

def show_about(event = None):
    messagebox.showwarning("About",
                           "Made in 2017")

root = Tk()

the_menu = Menu(root)

# File menu

file_menu = Menu(the_menu, tearoff = 0)

file_menu.add_command(label = "Open")
file_menu.add_command(label = "Save")
file_menu.add_separator()
file_menu.add_command(label = "Quit", command = quit_app)

the_menu.add_cascade(label = "File", menu = file_menu)

# Font menu

text_font = StringVar()
text_font.set("Times")

def change_font(event = None):
    print("Font picked: ", text_font.get())

font_menu = Menu(the_menu, tearoff = 0)

font_menu.add_radiobutton(label = "Time", variable = text_font, command = change_font)
font_menu.add_radiobutton(label = "Courier", variable = text_font, command = change_font)
font_menu.add_radiobutton(label = "Arial", variable = text_font, command = change_font)

# View menu

view_menu = Menu(the_menu, tearoff = 0)

line_numbers = IntVar()
line_numbers.set(1)

view_menu.add_checkbutton(label = "Line Numbers", variable = line_numbers)

view_menu.add_cascade(label = "Fonts", menu = font_menu)

the_menu.add_cascade(label = "View", menu = view_menu)

# Help menu

help_menu = Menu(the_menu, tearoff = 0)

help_menu.add_command(label = "About", 
                      accelerator = "Ctrl+H", 
                      command = show_about)

the_menu.add_cascade(label = "Help", menu = help_menu)

root.bind("<Control-H>", show_about)
root.bind("<Control-h>", show_about)

root.config(menu = the_menu)

root.mainloop()