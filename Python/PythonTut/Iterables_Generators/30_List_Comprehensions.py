print([2 * x for x in range(1,11)])

print([x for x in range(1,11) if x % 2 != 0])

# Generate 50 values, take to the power of 2, return multiples of 8
print([i ** 2 for i in range(50) if i % 8 == 0])

print([x * y for x in range(1, 3) for y in range(11, 16)])

# Generate a list of 10 values, multiply the by 2, return multiples of 8
print([x for x in [i * 2 for i in range(10)] if x % 8 == 0])

import random
print([x for x in [random.randint(1, 1000) for i in range(50)] if x % 9 == 0])

multidimList = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

print([col[1] for col in multidimList])
print([multidimList[i][i] for i in range(len(multidimList))])