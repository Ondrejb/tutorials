import sqlite3
import sys

def printDB():
    try:
        result = theCursor.execute("SELECT ID, FName, LName, Age, Address, Salary, HireDate FROM Employees")

        for row in result:
            print("ID: ", row[0])
            print("FName: ", row[1])
            print("LName: ", row[2])
            print("Age: ", row[3])
            print("Address: ", row[4])
            print("Salary: ", row[5])
            print("HireDate: ", row[6])

    except sqlite3.OperationalError as ex:
        print(ex.args)

    except Exception as e:
        print("Unknown Error")
        print(e.args)


db_conn = sqlite3.connect('test.db')

print("DB Created")

#printDB()

theCursor = db_conn.cursor()

db_conn.execute("DROP TABLE IF EXISTS Employees")
db_conn.commit()

try:
    db_conn.execute("CREATE TABLE Employees(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, FName TEXT NOT NULL, LName TEXT NOT NULL, Age INTEGER NOT NULL, Address TEXT, Salary REAL, HireDate TEXT);")
    db_conn.commit()

    print("Table Created")

except sqlite3.OperationalError:
    print("Table Couldn't be Created")

db_conn.execute("INSERT INTO Employees (Fname, LName, Age, Address, Salary, HireDate) VALUES ('Derek', 'Banas', 41, '123 Main St', 500000, date('now'))")

db_conn.commit()

printDB()

try:
    db_conn.execute("UPDATE Employees SET Address = '121 Main Street' WHERE ID=1")
    db_conn.commit()

except sqlite3.OperationalError as ex:
    print("Table couldn't be updated")
    print(ex.args)
    
printDB()

try:
    db_conn.execute("DELETE FROM Employees WHERE ID=1")
    #db_conn.commit()
    db_conn.rollback()

except sqlite3.OperationalError as ex:
    print("Failed to delete")
    print(ex.args)
    
printDB()

theCursor.execute("PRAGMA TABLE_INFO(Employees)")
rowNames = [nameTuple[1] for nameTuple in theCursor.fetchall()]

print(rowNames)

theCursor.execute("SELECT COUNT(*) FROM Employees")
numOfRows = theCursor.fetchall()

print("Total rows: ", numOfRows[0][0])

with db_conn:
    db_conn.row_factory = sqlite3.Row

    theCursor = db_conn.cursor()

    theCursor.execute("SELECT * FROM Employees")

    rows = theCursor.fetchall()

    for row in rows:
        print("{} {}".format(row["fName"], row["lName"]))

with open('dump.sql', 'w') as f:
    for line in db_conn.iterdump():
        f.write("%s\n" % line)

db_conn.close()

print("DB Closed")