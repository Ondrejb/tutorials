﻿import kivy
kivy.require('1.9.0')

from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import ObjectProperty
from kivy.core.window import Window
from kivy.uix.popup import Popup

class CustomPupup(Popup):
    pass

class SampBoxLayout(BoxLayout):
    checkbox_is_active = ObjectProperty(False)

    def checkbox_18_clicked(self, instance, value):
        if value:
            print("Checkbox is Checked")
        else:
            print("Checkbox is Unchecked")

    blue = ObjectProperty(True)
    red = ObjectProperty(False)
    green = ObjectProperty(False)

    def switch_on(self, instance, value):
        if value:
            print("Switch on")
        else:
            print("Switch off")

    def open_popup(self):
        the_popup = CustomPupup()
        the_popup.open()

    def spinner_clicked(self, value):
        print("Spinner Value: " + value)

class PopupApp(App):
    def build(self):
        Window.clearcolor = (1, 1, 1, 1)
        return SampBoxLayout()

app = PopupApp()
app.run()