import kivy
kivy.require('1.9.0')

from kivy.app import App
from kivy.uix.gridlayout import GridLayout

class SampleGridLayout(GridLayout):
    pass

class ToolbarApp(App):
    def build(self):
        return SampleGridLayout()

app = ToolbarApp()
app.run()