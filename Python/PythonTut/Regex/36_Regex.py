import re

str = "The ape was at the apex"

if re.search("ape", str):
    print("String contains ape")

allApes = re.findall("ape.", str)

for i in allApes:
    print(i)
    
# raw string
randStr = "Here is \\stuff"
print(r"Find \\stuff: ", re.search(r"\\stuff", randStr))

# word boundary
allApes = re.findall(r"\bape\b", str)

for i in allApes:
    print(i)

for i in re.finditer("ape.", str):
    locTuple = i.span()

    print(locTuple)
    print(str[locTuple[0]:locTuple[1]])

owlFood = "rat cat mat pat"

regex = re.compile("[cr]at")

owlFood = regex.sub("owl", owlFood)
print(owlFood)

longStr = '''This is a long
string that goes
on for many lines
'''

# targets each line
regex = re.compile(r"(?m)^.*?\s")
matches = re.findall(regex, longStr)

for i in matches:
    print(i)

print(longStr)

regex = re.compile("\n")
longStr = regex.sub(" ", longStr)
print(longStr)


xml = "<name>Life on Mars</name><name>Freaks and Geeks</name>"

# lazy match (.*?)
regex = re.compile("<name>(.*?)</name>")

matches = re.findall(regex, xml)

for i in matches:
    print(i)

number = "My number is 412-555-1212"

regex = re.compile("412-(.*)?-(.*)?")

matches = re.findall(regex, number)

print(matches[0][0])
print(matches[0][1])