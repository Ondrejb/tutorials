import re

birthday = "25-07-1993"

bdRegex = re.search(r"(\d{1,2})-(\d{1,2})-(\d{4})", birthday)

print("You were born on ", bdRegex.group())
print("Day: ", bdRegex.group(1))
print("Month ", bdRegex.group(2))
print("Year ", bdRegex.group(3))


match = re.search(r"\d{2}", "The chicked weighed 13 lbs")

print("Match: ", match.group())
print("Span: ", match.span())
print("Start: ", match.start())
print("End: ", match.end())


randStr = "December 21 1974"
regex = r"^(?P<month>\w+)\s(?P<day>\d{1,2})\s(?P<year>\d{4})"
matches = re.search(regex, randStr)

print("Day: ", matches.group("day"))
print("Month: ", matches.group("month"))
print("Year: ", matches.group("year"))