import re

# Back referemce
randStr = "<a href='#'><b>The link</b></a>"
regex = re.compile(r"<b>(.*?)</b>")
randStr = re.sub(regex, r"\1", randStr)
print(randStr)

telNum = "412-555-1212"
regex = re.compile(r"([\d]{3})-([\d]{3}-[\d]{4})")
telNum = re.sub(regex, r"(\1)\2", telNum)
print(telNum)

url = "https://www.youtube.com http://www.google.com"
regex = re.compile(r"(https?://([\w.]+))?")
url = re.sub(regex, r"<a href='\1'>\2</a>\r", url)
print(url)

# Look Ahead
randStr = "One two three four"
regex = re.compile(r"\w+(?=\b)")
matches = re.findall(regex, randStr)

for i in matches:
    print(i)

# Look behind
randStr = "1. Bread 2. Apples 3. Lettuce"
regex = re.compile(r"(?<=\d.\s)\w+")
matches = re.findall(regex, randStr)

for i in matches:
    print(i)

# Both
randStr = "<h1>I'm important</h1> <h1>So am I</h1>"
regex = re.compile(r"(?<=<h1>).+?(?=</h1>)")
matches = re.findall(regex, randStr)

for i in matches:
    print(i)

# Negative Look Ahead
randStr = "8 Apples = $3, 1 Bread $1, 1 Cereal $4"
regex = re.compile(r"(?<!\$)\d+")
matches = re.findall(regex, randStr)

matches = [int(i) for i in matches]

from functools import reduce
print("Total Items: {}".format(reduce(lambda x, y: x + y, matches)))