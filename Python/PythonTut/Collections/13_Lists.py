import random
import math

randList = ["string", 1.234, 28]

oneToTen = list(range(10))

randList += oneToTen

print("List Length: ", len(randList))

first3 = randList[0:3]

for i in first3:
    print("{} : {}".format(first3.index(i), i))

print("string" in first3)
print("Index of string: ", first3.index("string"))
print("How my string: ", first3.count("strign"))

first3[0] = "New String"

first3.append("another")

for i in first3:
    print("{} : {}".format(first3.index(i), i))

randNumbers = []

for i in range(5):
    randNumbers.append(random.randint(1,10))

randNumbers.sort()

randNumbers.insert(5, 10)
randNumbers.remove(10)

for i in randNumbers:
    print(i)

#list comprehensions

evenList = [i * 2 for i in range(10)]

for i in evenList:
    print(i)

numList = [1,2,3,4,5]

listOfValues = [[math.pow(m, 2), math.pow(m, 3), math.pow(m, 4)]
                for m in numList]

for i in listOfValues:
    print(i)

multiDList = [[0] * 10 for i in range(10)]
multiDList[0][1] = 10

print(multiDList[0][1])


listTable = [[0] * 4 for i in range(4)]

for i in range(4):
    for j in range(4):
        listTable[i][j] = "{} : {}".format(i, j)

for i in range(4):
    for j in range(4):
        print(listTable[i][j], end = " || ")
    print()

basic = range(1, 10)

multTable = [[i * j for i in range(1, 10)] for j in range(1, 10)]

for i in range(9):
    for j in range(9):
        print(multTable[i][j], end = ", ")
    print()