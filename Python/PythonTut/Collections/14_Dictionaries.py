derekDict = {"fName": "Derek", "lName": "Banas",
             "address" : "123 Main Street"}

print("First name", derekDict["fName"])

derekDict["address"] = "213 North St"

derekDict['city'] = 'Pittsburgh'

print(derekDict)

print("Is there a city: ", "city" in derekDict)
print(derekDict.values())
print(derekDict.keys())

for k, v in derekDict.items():
    print(k, v)

print(derekDict.get("mName", "Not Here"))

del derekDict["fName"]

for i in derekDict:
    print(i)

derekDict.clear()

