miles = int(input('Enter miles: '))
if miles < 0:
    print("Miles can't be less than zero")
else:
    print("{} miles equals {} kilometers".format(miles, miles * 1.60934))