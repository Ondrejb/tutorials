import os

with open("mydata.txt", 'w', encoding="utf-8") as myFile:
    myFile.write("Some random text\nMore random text\nAnd some more")

with open("mydata.txt", encoding="utf-8") as myFile:
    print(myFile.read())

print(myFile.closed)
print(myFile.name)
print(myFile.mode)

#os.remove("mydata2.txt")
#os.rename("mydata.txt", "mydata2.txt")
#os.remove("mydata2.txt")
#os.mkdir("mydir")
#os.chdir("mydir")

print("Curr dir: ", os.getcwd())

#os.chdir("..")

wordsCount = 0
charCount = 0
   
lineNum = 1

with open("mydata.txt", 'r', encoding="utf-8") as myFile:

    while True:
        line = str(myFile.readline()).strip()

        if not line:
            break

        print("Line", lineNum, " : ", line)
        words = line.split()
        wordsCount += len(words)

        for word in words:
            charCount += len(word)

        lineNum += 1

print()

print("Avg words: ", wordsCount / (lineNum-1))
print("Avg char count: ", charCount / wordsCount)