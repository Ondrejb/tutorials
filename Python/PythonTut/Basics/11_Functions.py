gbl_name = "Sally"

def change_name():
    global gbl_name
    gbl_name = "Sammy"

change_name()

print(gbl_name)


def solveForX(equation):
    x, add, num1, equal, num2 = equation.split()
    num1, num2 = int(num1), int(num2)
    return "x = " + str(num2 - num1)

print(solveForX("x + 4 = 9"))

# Return multiple values
def mult_divide(num1, num2):
    return (num1 * num2), (num1 / num2)

def isPrime(num):
    for i in range(2, num):
        if(num % i) == 0:
            return False

    return True

def getPrimes(max_number):
    listOfPrimes = []

    for num1 in range(2, max_number):
        if(isPrime(num1)):
           listOfPrimes.append(num1)

    return listOfPrimes

for prime in getPrimes(100):
    print(prime, end=", ")

def sumAll(*args):
    sum = 0

    for i in args:
        sum += i

    return sum

print("Sum: ", sumAll(1,2,3,4,5))