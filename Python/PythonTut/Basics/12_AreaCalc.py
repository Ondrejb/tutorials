import math

def rectangle_area():
    length = float(input("Enter the lenght: "))
    width = float(input("Enter the width: "))

    area = length * width

    print("Area of the rectangle is: ", area)

def circle_area():
    radius = float(input("Enter the radius: "))

    area = math.pi * radius**2

    print("Area of the circle is {:.2f}: ".format(area))

def get_area(shape):
    shape = shape.lower()

    if(shape == "rectangle"):
        rectangle_area()
    elif(shape == "circle"):
        circle_area()
    else:
        print("Please enter rectangle or circle")


def main():
    shape_type = input("Get area for what shape: ")

    get_area(shape_type)


main()