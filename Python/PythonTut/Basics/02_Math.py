﻿import math
from decimal import Decimal as D

num1, num2 = input('Enter 2 numbers: ').split()

num1 = int(num1)
num2 = int(num2)

sum = num1 + num2
sub = num1 - num2
mul = num1 * num2
div = num1 / num2
mod = num1 % num2

print("{} + {} = {}".format(num1, num2, sum))
print("{} - {} = {}".format(num1, num2, sub))
print("{} * {} = {}".format(num1, num2, mul))
print("{} / {} = {}".format(num1, num2, div))
print("{} % {} = {}".format(num1, num2, mod))

math.ceil(1.1)
math.floor(1.1)
math.fabs(1.1)
math.factorial(10)

math.e
math.pi

sum = D(0)
sum += D("0.1")
sum += D("0.1")
sum += D("0.1")
sum += D("-0.3")
print("Decimal sum: ", sum)
print("Normal sum: ", 0.1 + 0.1 + 0.1 - 0.3)