money, interest = input("Enter original amount and interest: ").split()

money = float(money)
interest = float(interest)

for i in range(10):
    money += money * interest

print("Amount after 10 years: {:.2f}".format(money))