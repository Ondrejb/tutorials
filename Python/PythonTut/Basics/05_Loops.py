﻿import random

print("-- FOR --")
for i in range(2, 10):
    print(i)

for i in [0,2,4,6,8]:
    print(i)

rand_num = random.randrange(1, 51)
i = 1

print("-- WHILE--")

while(i != rand_num):
    i+=1

print(i)

print("-- DO WHILE--")

