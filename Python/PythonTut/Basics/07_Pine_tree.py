rows = int(input("How tall is the tree: "))

for i in range(rows):
    str = " " * (rows - i) + "#" * (i * 2 - 1) 
    print(str)

print(" " * (rows - 1) + "#")