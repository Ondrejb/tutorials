﻿samp_string = "This is a very important string"

print(samp_string[0])
print(samp_string[-1]) #last string
print(samp_string[-5])

print("Length: ", len(samp_string))
print(samp_string[0:4])
print(samp_string[8:])

num_string = str(4)

for char in samp_string:
    print(char)

for i in range(0, len(samp_string)-1, 2):
    print(samp_string[i] + samp_string[i+1])

string = "HELLO" 
#string = input("Enter string to hide: ")

string = string.upper()

hidden = ""

for c in string:
    hidden += str(ord(c))

print("Hidden: ", hidden)

for i in range(0, len(hidden)-1, 2):
    unicode = int(hidden[i] + hidden[i+1])
    print(chr(unicode), end="")

print("\nGG WP")


rand_string = "    this is an important string   "
rand_string = rand_string.lstrip()
rand_string = rand_string.rstrip()
rand_string = rand_string.strip()

print(rand_string.capitalize())

a_list = ["Bunch", "of", "random", "words"]

print(" ".join(a_list))

print("How many \"is\": ", rand_string.count("is"))

print("Where is \"string\": ", rand_string.find("string"))

print(rand_string.replace("an ", "a kind of "))

letter_z = "z"
num_3 = "3"
space = " "

print(letter_z.isalnum())
print(letter_z.isalpha())
print(space.isspace())

def isFloat(str_val):
    try:
        float(str_val)
        return True
    except ValueError:
        return False

print("Is Pi a float: ", isFloat("3.1415"))