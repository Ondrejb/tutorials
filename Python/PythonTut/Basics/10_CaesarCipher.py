def Encrypt(string, shift):
    shift = shift % 26
    encrypted = ""

    for c in string:
        if(c.isalpha()):
            encrypted += shiftChar(c, shift)
        else:
            encrypted += c

    return encrypted

def Decrypt(string, shift):
    shift = shift % 26
    decrypted = ""

    for c in string:
        if(c.isalpha()):
            decrypted += shiftChar(c, -1*shift)
        else:
            decrypted += c

    return decrypted

def shiftChar(char, shift):
    code = ord(char)
    if(char.islower()):
        if(code + shift > 122):
            return chr(97 + (code + shift - 123))
        if(code + shift < 97):
            return chr(123 - (97 - (code + shift)))
        else:
            return chr(code + shift)
    if(char.isupper()):
        if(code + shift > 90):
            return chr(65 + (code + shift - 91))
        if(code + shift < 65):
            return chr(91 - (65 - (code + shift)))
        else:
            return chr(code + shift)

print(Encrypt("AHOJ", 1))
print(Decrypt(Encrypt("AHOJ", 1), 1))

print(Encrypt("AaXZz", 50))
print(Decrypt(Encrypt("AaXZz", 50), 50))

print(Encrypt("Make me secret", 4))
print(Decrypt(Encrypt("Make me secret", 4), 4))