import random

class Warrior:

    def __init__(self, name="", health=100, attackMax=10, blockMax=5):
        self.name = name
        self.health = health
        self.attackMax = attackMax
        self.blockMax = blockMax



class Battle:
    
    def battle(warrior1 = Warrior(), warrior2 = Warrior()):

        while(warrior1.health > 0 and warrior2.health > 0):
            w1Dmg = max(0, int(random.random() * warrior1.attackMax - random.random() * warrior2.blockMax))
            warrior2.health -= w1Dmg
            print("{} does {} damage to {}, {} health remaining".format(warrior1.name, w1Dmg, warrior2.name, warrior2.health))

            w2Dmg = max(0, int(random.random() * warrior2.attackMax - random.random() * warrior1.blockMax))
            warrior1.health -= w2Dmg
            print("{} does {} damage to {}, {} health remaining".format(warrior2.name, w2Dmg, warrior1.name, warrior1.health))

        print("Battle is over!")

        if(warrior1.health > 0):
            print(warrior1.name, " is victorius")
        elif(warrior2.health > 0):
            print(warrior2.name, " is victorius")
        else:
            print("They killed each other!")

def main():
    warrior1 = Warrior("Joe")
    warrior2 = Warrior("Sam")

    Battle.battle(warrior1, warrior2)

main()