class Animal:

    def __init__(self, birthType="Unknown",
                 appearance="Unknown", blooded="Unknow"):
        self.birthType = birthType
        self.appearance = appearance
        self.blooded = blooded

    @property
    def birthType(self):
        return self.__birthType

    @birthType.setter
    def birthType(self, birthType):
        self.__birthType = birthType

    @property
    def appearance(self):
        return self.__appearance

    @appearance.setter
    def appearance(self, appearance):
        self.__appearance = appearance

    @property
    def blooded(self):
        return self.__blooded

    @blooded.setter
    def blooded(self, blooded):
        self.__blooded = blooded

    def __str__(self):
        return "A {} is {} it is {} it is {}".format(type(self).__name__, self.birthType, self.appearance, self.blooded)

class Mammal(Animal):

    def __init__(self, birthType = 'Born alive', appearance = 'Hair or fur', blooded = 'Warm blooded', nurseYoung=True):        
        Animal.__init__(self, birthType, appearance, blooded)
        self.nurseYoung = nurseYoung
                
    @property
    def nurseYoung(self):
        return self.__nurseYoung

    @nurseYoung.setter
    def nurseYoung(self, nurseYoung):
        self.__nurseYoung = nurseYoung

    def __str__(self):
        return super().__str__() + " and it is {} they nurse"\
            " their young".format(self.nurseYoung)

class Reptile(Animal):

    def __init__(self, birthType = 'Born in an egg or alive', appearance = 'Dry scales', blooded = 'Cold blooded'):        
        Animal.__init__(self, birthType, appearance, blooded)

def getBirthType(theObject):
    print("the {} is {}".format(type(theObject).__name__, theObject.birthType))

def main():
    animal1 = Animal("Born Alive")
    print(animal1.birthType)
    print(animal1)

    mammal = Mammal()
    print(mammal)

    reptile = Reptile()
    print(reptile)

    getBirthType(reptile)

main()