class Square:

    def __init__(self, height="0", width="0"):
        self.height = height
        self.width = width

    @property
    def height(self):
        print("Retrieving the Height")

        return self.__height

    @height.setter
    def height(self, value):
        if value.isdigit():
            self.__height = value
        else:
            print("Please only enter numbers for Height")

    @property
    def width(self):
        print("Retrieving the Width")

        return self.__width

    @width.setter
    def width(self, value):
        if value.isdigit():
            self.__width = value
        else:
            print("Please only enter numbers for Width")

    def getArea(self):
        return int(self.__width) * int(self.__height)

def main():
    square = Square()

    height = input("Please enter height: ")
    width = input("Please enter width: ")

    square.height = height
    square.width = width

    print("Area: ", square.getArea())

main()