class Time:

    def __init__(self, hour=0, minute=0, second=0):
        self.hour = hour
        self.minute = minute
        self.second = second

    def __str__(self):
        return "{}:{:02d}:{:02d}".format(self.hour, self.minute, self.second)

    def __add__(self, other_time):
        new_time = Time()
                
        seconds = self.second + other_time.second
        if(seconds >= 60):
            new_time.minute += 1
            new_time.second += seconds - 60
        else:
            new_time.second += seconds
       
        minutes = self.minute + other_time.minute
        if(minutes >= 60):
            new_time.hour += 1
            new_time.minute += minutes - 60
        else:
            new_time.minute += minutes
            
        hours = self.hour + other_time.hour + new_time.hour
        if(hours >= 24):
            new_time.hour = hours - 24
        else:
            new_time.hour = hours

        return new_time

def main():
    t1 = Time(12, 15, 17)
    t2 = Time(12, 46, 56)

    print(t1+t2)

main()