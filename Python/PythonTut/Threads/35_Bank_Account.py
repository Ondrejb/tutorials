import threading
import time
import random

class BankAccount(threading.Thread):

    accountBallance = 100

    def __init__(self, name, moneyRequest):
        threading.Thread.__init__(self)

        self.name = name
        self.moneyRequest = moneyRequest

    def run(self):
        threadLock.acquire()

        BankAccount.getMoney(self)

        threadLock.release()

    @staticmethod
    def getMoney(customer):
        print("{} tries to withdrawal ${} at {}".format(customer.name, 
                                                        customer.moneyRequest, 
                                                        time.strftime("%H:%M:%S", time.gmtime())))
        if BankAccount.accountBallance - customer.moneyRequest > 0:
            BankAccount.accountBallance -= customer.moneyRequest
            print("New acc balance: ${}".format(BankAccount.accountBallance))
        else:
            print("Not enough money")
            print("Acc balance: ${}".format(BankAccount.accountBallance))

        time.sleep(3)

threadLock = threading.Lock()

doug = BankAccount("Doug", 1)
paul = BankAccount("Paul", 100)
sally = BankAccount("Sally", 50)

doug.start()
paul.start()
sally.start()

doug.join()
paul.join()
sally.join()

print("Execution ends")