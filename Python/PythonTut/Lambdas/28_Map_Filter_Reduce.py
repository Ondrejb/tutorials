﻿# MAP

oneTo10 = range(1,11)

def dbl_num(num):
    return num * 2

print(list(map(dbl_num, oneTo10)))

print(list(map(lambda x: x*3, oneTo10))) 


l = list(map(lambda x, y: x + y, [1, 2, 3], [1, 2, 3]))
print(l)

# FILTER

import random

print(list(filter(lambda x: x % 2 == 0, range(1,11))))
print(list(filter(lambda x: x % 9 == 0, [random.randint(1, 1000) for i in range(100)])))

# REDUCE

from functools import reduce

print(reduce(lambda x, y: x + y, range(1, 6)))