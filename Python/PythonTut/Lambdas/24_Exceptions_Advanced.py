class DogNameError(Exception):

    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)

try:
    dogName = input("What is your dog's name: ")

    if any(char.isdigit() for char in dogName):
        raise DogNameError

except DogNameError:
    print("Dog's name can't contain a number")

else: #executes if no exeception happened
    print("All good")

finally:
    print("You will always see this")