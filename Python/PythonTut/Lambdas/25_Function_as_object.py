def mult_by_2(num):
    return num * 2

times_two = mult_by_2

print("4 * 2 = ", times_two(4))


def do_math(func, num):
    return func(num)

print("8 * 2 = ", do_math(times_two, 8))

def get_func_mult_by_num(num):

    def mult_by(value):
        return num * value

    return mult_by

generated_func = get_func_mult_by_num(5)

print("5 * 10 = ", generated_func(10))

listOfFunc = [times_two, do_math, generated_func]

def getOdd(numbers, oddTestFunc):
    odd = []

    for num in numbers:
        if(oddTestFunc(num)):
            odd.append(num)

    return odd

def isOdd(num):
    return num % 2 == 1

print(getOdd([1,2,3,5,7,8,9], isOdd))