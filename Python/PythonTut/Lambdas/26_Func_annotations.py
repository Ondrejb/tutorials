# Not enforced, for documentation only

def random_func(name: str, age: int, weight: float) -> str:
    print("Name: ", name)
    print("Age: ", age)
    print("Weight: ", weight)

print(random_func("Derek", 41, 165.5))

# OK
print(random_func(5, "Prdel", None))

print(random_func.__annotations__)