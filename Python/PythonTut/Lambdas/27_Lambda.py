sum = lambda x, y: x + y

print(sum(4, 5))

can_vote = lambda age: True if age >= 18 else False

print(can_vote(17))
print(can_vote(19))

powerList = [lambda x: x**2, 
             lambda x: x**3, 
             lambda x: x**4, 
             lambda x: x**5]

for fun in powerList:
    print(fun(2))

attack = {"quick": (lambda: print("Quick Attack")),
          "power": (lambda: print("Power Attack")),
          "miss": (lambda: print("Missed!"))}

import random

for i in range(10):
    random.choice(list(attack.values()))()
    
headsTails = [random.choice(["H", "T"]) for i in range(100)]

print("Heads: ", headsTails.count("H"))
print("Tails: ", headsTails.count("T"))