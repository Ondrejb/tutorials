file = io.open("test.lua", "w+")

file:write("Random string of text \n")
file:write("Some more text\n")

file:seek("set", 0)

print(file:read("*a"))

file:close()

file = io.open("test.lua", "a+")
file:write("Even more text\n")

file:seek("set", 0)

print(file:read("*a"))

file:close()
