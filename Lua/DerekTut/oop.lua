Animal = {height = 0, weight = 0, name = "No name", sound = "No sound"}

function Animal:new (height, weight, name, sound)
	setmetatable({}, Animal)

	self.height = height
	self.weight = weight
	self.name = name
	self.sound = sound

	return self
end

function Animal:toString()
	animalStr = string.format("%s weights %.1f lbs, is %.1f\" tall and says %s", self.name, self.weight, self.height, self.sound)

	return animalStr
end

spot = Animal:new(10, 15, "Spot", "Woof")

print(spot.weight)
print(spot:toString())

Cat = Animal:new()

function Cat:new (height, weight, name, sound, favouriteFood)
	setmetatable({}, Cat)

	self.height = height
	self.weight = weight
	self.name = name
	self.sound = sound
	self.favouriteFood = favouriteFood

	return self
end


function Cat:toString()
	animalStr = string.format("%s weights %.1f lbs, is %.1f\" tall, says %s and loves %s", self.name, self.weight, self.height, self.sound, self.favouriteFood)

	return animalStr
end

fluffy = Cat:new(8, 7,  "Fluffy", "Meow", "Tuna")
print()
print(fluffy:toString())
print()

print(spot:toString())


print("\n")
