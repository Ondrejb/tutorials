longString = [[
 I am a very long
 string that
 goes on
 forever]]

 io.write(longString, "\n")

 longString = longString .. " " .. name

io.write("Concat: ", longString, "\n")

quote = "I changed my password everywhere to 'incorrect.' That way when I forget it, it always reminds me, 'Your password is incorrect.'"

io.write("Quote length: ", string.len(quote), "\n")
io.write("Quote length: ", #quote, "\n")
io.write("Replace I with me: ", string.gsub(quote, "I", "me"), "\n")
io.write("Index of \"password\": ", string.find(quote, "password"), "\n")
