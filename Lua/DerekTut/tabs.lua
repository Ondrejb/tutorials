tab = {}

for i = 1, 10 do
	tab[i] = i
end

io.write("First: ", tab[1], "\n")
io.write("Number of items: ", #tab, "\n")

table.insert(tab, 1, 0)
io.write("First: ", tab[1], "\n")

print(table.concat(tab, ", "))

table.remove(tab, 1)

print(table.concat(tab, ", "))

multiTab = {}

for i = 0, 9 do
	multiTab[i] = {}
	for j = 0, 9 do
		multiTab[i][j] = tostring(i) .. tostring(j)
	end
	print()
end


for i = 0, 9 do
	for j = 0, 9 do
		io.write(multiTab[i][j], " ")
	end
	print()
end

io.write("multiTab[0][0]: ", multiTab[0][0], "\n")
