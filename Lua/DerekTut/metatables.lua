tbl = {}

for x = 1, 10, 1 do
	tbl[x] = x
end

for i = 1, #tbl do
	print(tbl[i] .. " ")
end

tbl2 = {}

for x = 1, 10, 1 do
	tbl2[x] = x+10
end

print()

for i = 1, #tbl2 do
	print(tbl2[i] .. " ")
end

print()

mt = {
	__add = function(table1, table2)
		sumTable = {}

		for y = 1, #table1 do
			if((table1[y] ~= nil) and (table2[y] ~= nil)) then
				sumTable[y] = table1[y] + table2[y]
			else
				sumTable[y] = 0
			end
		end

		return sumTable
	end,

	__eq = function(table1, table2)
		return table1.value == table2.value
	end
}

setmetatable(tbl, mt)

print(tbl == tbl)

setmetatable(tbl2, mt)

print(tbl == tbl2)

sumTbl = {}
sumTbl = tbl + tbl2

for i = 1, #sumTbl do
	print(sumTbl[i] .. " ")
end
