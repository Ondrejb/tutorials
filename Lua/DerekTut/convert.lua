local convert = {}

function convert.feetToCentimeter(feet)
	return feet * 30.48
end

return convert
