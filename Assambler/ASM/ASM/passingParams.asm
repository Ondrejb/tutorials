
.code
; param	qword	dw	w	b
; 1st	rcx		ecx	cx	cl
; 2nd	rdx		edx	dx	dl
; 3rd	r8		r8d	r8w	r8b
; 4th	r9		r9d	r9w	r9b
; >4  stack --

PassingParam proc
	mov eax, ecx	; an int (dword) is passed as the first param, move it to eax
	neg eax			; negate it
	ret
PassingParam endp

Min proc
	mov eax, ecx	; assume ecx is smaller
	cmp edx, eax	; compare to edx
	jg ReturnEax

	mov eax, edx
	ret

ReturnEax:
	ret
Min endp

Sum proc
	mov ax, cx
	add ax, dx
	add ax, r8w
	add ax, r9w
	ret
Sum endp
end