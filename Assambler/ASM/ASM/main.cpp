#include <iostream>

#include "zeroArray.h"

 extern "C" int GetValueFromASM();
 extern "C" int TestFunction();
 extern "C" int JumpsTestFunction();
 extern "C" int PassingParam(int a);
 extern "C" int Min(int a, int b);
 extern "C" short Sum(short a, short b, short c, short d);

// Must be 32 bit
//int GetValueFromASMInline()
//{
//	_asm
//	{
//		mov eax, 39		
//	}
//}

int main()
{
	// std::cout << "Sup, ASM said: " << GetValueFromASMInline() << "\n";
	std::cout << "Sup, ASM said: " << GetValueFromASM() << "\n";
	std::cout << "Test function: " << TestFunction() << "\n";
	JumpsTestFunction();
	std::cout << "Function returned: " << PassingParam(5) << "\n";
	std::cout << "Min: " << Min(-5, 2) << "\n";
	std::cout << "Sum: " << Sum(1, 2, 3, 4) << "\n";

	auto count = 1024;
	auto arr = new char[count];

	// Fill in the array
	for (auto i = 0; i < count; i++)
		arr[i] = static_cast<char>(rand());

	// Output the array
	for (auto i = 0; i < count; i++)
		std::cout << arr[i] << " ";

	ZeroArray(arr, count * sizeof(char));

	// Output the array, should be all zeros
	for (auto i = 0; i < count; i++)
		std::cout << arr[i] << " ";

	delete[] arr;

	return 0;
}