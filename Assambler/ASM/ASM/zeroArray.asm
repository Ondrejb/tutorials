.code
; void ZeroArray(void* rcx, int edx)
; sets all bytes from *rcx to *rcx+edx to 0

ZeroArray proc
	cmp edx, 0		; check for 0 or less
	jle Finished	; if true, just return

	cmp edx, 1		; check for 1
	je SetFinalByte

	mov ax, 0		; set ax to 00
	mov r8d, edx	; save the original count to r8d
	shr edx, 1		; halve the count because we are using ax, not al

MainLoop:
	mov word ptr [rcx], ax	; sets two bytes to 0
	add rcx, 2				; move rcx to the next two bytes
	dec edx					; decrement counter
	jnz MainLoop			; jump if we have more to set

	and r8d, 1				; check if there was an even number
	jz Finished				; if true, we are done

SetFinalByte:
	mov byte ptr[rcx], 0

Finished:
	ret
ZeroArray endp
end