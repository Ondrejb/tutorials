.data
mybyte db 0

.code
TestFunction proc
	mov rax, 10		; move 10 to rax
	add rax, 5		; add 5 to rax
	sub rax, 2		; subtract 2 from rax
	neg rax			; negate rax
	inc rax			; increment rax
	dec mybyte		; decrement memory (mybyte)

	mov rbx, 2		; move 2 to rbx
	xchg rax, rbx	; exchange rax and rbx	

	lea rax, mybyte ; load address

	ret				; pop instruction pointer to where it was before the function call
TestFunction endp


JumpsTestFunction proc
	mov ecx, 10
MainLoop:
	dec ecx			; decrement ecx
	jnz MainLoop	; jump to MainLoop if not zero

	; Unconditional jump
	jmp MyLabel		; jump to a label
	mov eax, 25		; will be skipped

MyLabel:
	mov ecx, 89
	mov ebx, 56
	cmp ecx, ebx	; compare register values, save to flags register

	; je - jump equal
	; jne - jump not equal
	; jl <, jle <=, jg >, jge >=
	
	jg MyLabel2		; jump to MyLabel2 if exc > ebx
	mov eax, 1		; may be skipped
MyLabel2:
	ret
JumpsTestFunction endp

GetValueFromASM proc
	mov rax, 23783
	ret
GetValueFromASM endp
end