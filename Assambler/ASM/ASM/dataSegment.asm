.data
; [label] [size] [initialValue / ?]
; size: 
;	Byte (db, byte, sbyte)
;	Word (dw, word, sword)
;	Double Word (dd, dword, sdword)
;	Quad Word (dq, qword, sqword)

;	xmmword: xmmword - SSE, 128 bits, 16 bytes
myXmmWord xmmword ? ; can't use initial val, has to be ?

;	ymmword: ymmword - AVX, 256 bits, 32 bytes
myYmmWord ymmword ? ; can't use initial val, has to be ?

;	float (real4), a.k.a Single
;	double (real8), a.k.a. Double
;	?? (real10 - x87 floating point unit)

myFloat real4 10.0	; must use the .

myByte byte 0

myDWord dd "ABCD"					; a double word, representing the ABCD string

myByteArray db 1000 dup (0)			; an array of 1000 bytes, all set to 0
myByteArray2 db 1, 2, 3, 4			; an array of four bytes
myByteArray3 db "wgsdfgfdsdfg", 0	; null terminated string

myWordArray dw 10 dup (7 dup (6))	; 2D array, 70 words, each word set to 6

myOtherArray db 10 dup (1, 2, 3, 4)	; an array of 40 elements - 1, 2, 3, 4, 1, 2, 3, 4, ...

.code
DataSegmentProc proc
	ret
DataSegmentProc endp
end