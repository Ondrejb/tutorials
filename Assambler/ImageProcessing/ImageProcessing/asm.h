#pragma once

extern "C" void ASMAdjustBrightness(
	unsigned char* bmpDataScan0,
	unsigned char* bmpOriginal,
	short value,
	int imageSizeInBytes
);