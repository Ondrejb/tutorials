.data


.code
; extern "C" void ASMAdjustBrightness(
;	unsigned char* bmpDataScan0,	= RCX
;	unsigned char* bmpOriginal,		= RDX
;	short value,					= R8W
;	int imageSizeInBytes);			= R9D
ASMAdjustBrightness proc
	mov r10, 0					; set the offset pointer to 0
	cmp r8w, 0					; check if we are subtracting or adding
	jl SubtractBrightness
	
	mov r11w, 0ffffh			; set my overflow value to 255
	
MainLoopAdd:
	mov al, byte ptr [rdx+r10]	; read the next byte from orig
	add al, r8b					; add the brightness
	cmovc ax, r11w				; set to 255 on overflow
	mov byte ptr [rcx + r10], al; store the anwer in scan0
	inc r10						; move on to the next byte (both arrays)
	dec r9d						; decrement counter
	jnz MainLoopAdd				; jump if there is more
	ret
	
SubtractBrightness:
	mov r11w, 0					; set my overflow value to 0 
	neg r8w						; negate the value as we're subbing
	
MainLoopSubtract:
	mov al, byte ptr [rdx+r10]	; read the next byte from orig
	sub al, r8b					; sub the brightness
	cmovc ax, r11w				; set to 0 on overflow
	mov byte ptr [rcx + r10], al; store the anwer in scan0
	inc r10						; move on to the next byte (both arrays)
	dec r9d						; decrement counter
	jnz MainLoopSubtract		; jump if there is more
	ret
ASMAdjustBrightness endp
end
