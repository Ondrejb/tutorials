#pragma once
#include <time.h>
#include "asm.h"

namespace ImageProcessing {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Drawing::Imaging;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		Bitmap^ bmpFront;
		unsigned char* bmpOriginal;
		static int imageSizeInBytes = -1;
		static Rectangle imageRectangle;
		BitmapData^ bmpData;
		static double cppCount = 0.0;
		static double cppTotal = 0.0;
		static double asmCount = 0.0;
		static double asmTotal = 0.0;

		void AdjustBrightness(unsigned char* bmp, short amount)
		{
			if(amount > 0)
			{
				for (auto i = 0; i < imageSizeInBytes; i++)
				{
					if ((bmpOriginal[i] + amount > 255))
						bmp[i] = 255;
					else
						bmp[i] = bmpOriginal[i] + amount;
				}
			} 
			else
			{
				for(auto i = 0; i < imageSizeInBytes; i++)
				{
					if (bmpOriginal[i] + amount < 0)
						bmp[i] = 0;					
					else
						bmp[i] = bmpOriginal[i] + amount;
				}
			}			
		}

		void ClearOriginalImage()
		{
			if (bmpOriginal != nullptr)
				delete[] bmpOriginal;
		}

		// Make a copy of the orig image
		void SaveOriginalImage(System::Drawing::Bitmap^ bmp)
		{
			ClearOriginalImage();

			imageSizeInBytes = bmp->Width * bmp->Height * 3;
			bmpOriginal = new unsigned char[imageSizeInBytes];
			imageRectangle.Width = bmp->Width;
			imageRectangle.Height = bmp->Height;

			bmpData = bmp->LockBits(imageRectangle, ImageLockMode::ReadOnly, PixelFormat::Format24bppRgb);

			unsigned char* p = (unsigned char*)bmpData->Scan0.ToPointer();

			for(int i = 0; i < imageSizeInBytes; i++)
			{
				bmpOriginal[i] = *p++;
			}

			bmp->UnlockBits(bmpData);
		}

		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	protected:
	private: System::Windows::Forms::ToolStripMenuItem^  fileToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  openToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem;
	private: System::Windows::Forms::PictureBox^  picImage;
	private: System::Windows::Forms::TrackBar^  trackBar1;
	private: System::Windows::Forms::Label^  lblCppAverage;
	private: System::Windows::Forms::OpenFileDialog^  dlgOpen;
	private: System::Windows::Forms::Label^  lblAsmAvg;
	private: System::Windows::Forms::TrackBar^  trkAsm;


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->openToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->picImage = (gcnew System::Windows::Forms::PictureBox());
			this->trackBar1 = (gcnew System::Windows::Forms::TrackBar());
			this->lblCppAverage = (gcnew System::Windows::Forms::Label());
			this->dlgOpen = (gcnew System::Windows::Forms::OpenFileDialog());
			this->lblAsmAvg = (gcnew System::Windows::Forms::Label());
			this->trkAsm = (gcnew System::Windows::Forms::TrackBar());
			this->menuStrip1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->picImage))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trkAsm))->BeginInit();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12));
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->fileToolStripMenuItem });
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(927, 24);
			this->menuStrip1->TabIndex = 0;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this->fileToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->openToolStripMenuItem,
					this->exitToolStripMenuItem
			});
			this->fileToolStripMenuItem->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9));
			this->fileToolStripMenuItem->Name = L"fileToolStripMenuItem";
			this->fileToolStripMenuItem->Size = System::Drawing::Size(39, 20);
			this->fileToolStripMenuItem->Text = L"&File";
			// 
			// openToolStripMenuItem
			// 
			this->openToolStripMenuItem->Name = L"openToolStripMenuItem";
			this->openToolStripMenuItem->Size = System::Drawing::Size(104, 22);
			this->openToolStripMenuItem->Text = L"&Open";
			this->openToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::openToolStripMenuItem_Click);
			// 
			// exitToolStripMenuItem
			// 
			this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
			this->exitToolStripMenuItem->Size = System::Drawing::Size(104, 22);
			this->exitToolStripMenuItem->Text = L"E&xit";
			this->exitToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::exitToolStripMenuItem_Click);
			// 
			// picImage
			// 
			this->picImage->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->picImage->Location = System::Drawing::Point(13, 28);
			this->picImage->Name = L"picImage";
			this->picImage->Size = System::Drawing::Size(902, 319);
			this->picImage->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->picImage->TabIndex = 1;
			this->picImage->TabStop = false;
			// 
			// trackBar1
			// 
			this->trackBar1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->trackBar1->Enabled = false;
			this->trackBar1->Location = System::Drawing::Point(12, 350);
			this->trackBar1->Maximum = 255;
			this->trackBar1->Minimum = -255;
			this->trackBar1->Name = L"trackBar1";
			this->trackBar1->Size = System::Drawing::Size(731, 45);
			this->trackBar1->TabIndex = 2;
			this->trackBar1->TickFrequency = 16;
			this->trackBar1->Scroll += gcnew System::EventHandler(this, &MyForm::trackBar1_Scroll);
			// 
			// lblCppAverage
			// 
			this->lblCppAverage->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->lblCppAverage->AutoSize = true;
			this->lblCppAverage->Location = System::Drawing::Point(749, 350);
			this->lblCppAverage->Name = L"lblCppAverage";
			this->lblCppAverage->Size = System::Drawing::Size(109, 20);
			this->lblCppAverage->TabIndex = 3;
			this->lblCppAverage->Text = L"C++ Average: ";
			// 
			// dlgOpen
			// 
			this->dlgOpen->Filter = L"JPeg|*.jpg|Bitmap|*.bmp|All Files|*.*";
			// 
			// lblAsmAvg
			// 
			this->lblAsmAvg->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->lblAsmAvg->AutoSize = true;
			this->lblAsmAvg->Location = System::Drawing::Point(750, 401);
			this->lblAsmAvg->Name = L"lblAsmAvg";
			this->lblAsmAvg->Size = System::Drawing::Size(115, 20);
			this->lblAsmAvg->TabIndex = 5;
			this->lblAsmAvg->Text = L"ASM Average: ";
			// 
			// trkAsm
			// 
			this->trkAsm->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->trkAsm->Enabled = false;
			this->trkAsm->Location = System::Drawing::Point(13, 401);
			this->trkAsm->Maximum = 255;
			this->trkAsm->Minimum = -255;
			this->trkAsm->Name = L"trkAsm";
			this->trkAsm->Size = System::Drawing::Size(731, 45);
			this->trkAsm->TabIndex = 4;
			this->trkAsm->TickFrequency = 16;
			this->trkAsm->Scroll += gcnew System::EventHandler(this, &MyForm::trkAsm_Scroll);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(9, 20);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(927, 476);
			this->Controls->Add(this->lblAsmAvg);
			this->Controls->Add(this->trkAsm);
			this->Controls->Add(this->lblCppAverage);
			this->Controls->Add(this->trackBar1);
			this->Controls->Add(this->picImage);
			this->Controls->Add(this->menuStrip1);
			this->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->MainMenuStrip = this->menuStrip1;
			this->Margin = System::Windows::Forms::Padding(4, 5, 4, 5);
			this->Name = L"MyForm";
			this->Text = L"Image Proc";
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &MyForm::MyForm_FormClosing);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->picImage))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trkAsm))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private:
		System::Void exitToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		{
			Application::Exit();
		}

		System::Void openToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		{
			if(dlgOpen->ShowDialog() == System::Windows::Forms::DialogResult::OK)
			{
				try
				{
					bmpFront = (Bitmap^)Image::FromFile(dlgOpen->FileName);
					SaveOriginalImage(bmpFront);
					picImage->Image = bmpFront;
					trackBar1->Enabled = true;
					trkAsm->Enabled = true;
					cppTotal = 0.0;
					cppCount = 0.0;
					asmTotal = 0.0;
					asmCount = 0.0;
				}
				catch(...)
				{
					MessageBox::Show("File could not be opened");
				}
			}
		}

		System::Void trackBar1_Scroll(System::Object^  sender, System::EventArgs^  e) 
		{
			bmpData = bmpFront->LockBits(imageRectangle, ImageLockMode::WriteOnly, PixelFormat::Format24bppRgb);

			long startTime = clock();
			AdjustBrightness((unsigned char*)bmpData->Scan0.ToPointer(), trackBar1->Value);
			long finishTime = clock();

			bmpFront->UnlockBits(bmpData);
			picImage->Image = bmpFront;

			cppTotal += finishTime - startTime;
			cppCount++;
			lblCppAverage->Text = "C++ Average: " + Math::Round(cppTotal / cppCount);
		}
	
		System::Void MyForm_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) 
		{
			ClearOriginalImage();
		}

		System::Void trkAsm_Scroll(System::Object^  sender, System::EventArgs^  e) 
		{
			bmpData = bmpFront->LockBits(imageRectangle, ImageLockMode::WriteOnly, PixelFormat::Format24bppRgb);

			long startTime = clock();
			ASMAdjustBrightness((unsigned char*)bmpData->Scan0.ToPointer(), bmpOriginal, trkAsm->Value, imageSizeInBytes);
			long finishTime = clock();

			bmpFront->UnlockBits(bmpData);
			picImage->Image = bmpFront;

			asmTotal += finishTime - startTime;
			asmCount++;
			lblAsmAvg->Text = "ASM Average: " + Math::Round(asmTotal / asmCount);
		}
};
}
